package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Path("/dataservice")
public class DataService {
	
	String host = "jdbc:mysql://localhost:3606/rondedesfacs";
	String user = "admin";
	String pass = "admin";
	
	private final String bdd = "jdbc:mysql://localhost:3306/rondedesfacs?user=admin&password=admin";
	
	private final String[] participants_cols = { "N� dossard", "Pr�nom", "Nom", 
			"Civilit�", "Ville", "Code Postal", 
			"R�gion", "Etablissement", "Course", "Inscription valid�e"};
	
	private final String[] courses_cols = { "Course", "Date de d�but d'inscription",
			"Date de fin d'inscription", "Date de d�roulement", "Nombre de relais", 
			"Distance entre relais", "Statut", "Nombre de participants (broken)", 
			"Nombre de participants max", "Nombre d'inscrit"};
	
	private final String[] resultat_cols = {"course", "rang", "nb_passages", 
			"temps", "prenom", "nom", "civilite", "ville",
			"cp", "region", "etablissement", "num_dossard"};
	
	/**
	 * Methode getParticipants :
	 * 
	 * Renvoie la liste des participants sous format JSON
	 * 
	 * @return
	 */
	@GET
	@Path("/participants")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getParticipants() {
		
		String result = new String();
		
		String query = "SELECT * FROM V_PARTICIPANTS";
		
		Connection conn = null;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection(bdd);
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			System.out.println("Driver non d�finit");
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		if ( conn != null ) {
			
			Statement statement;
			
			try {
				
				statement = conn.createStatement();
				
				ResultSet datas = statement.executeQuery(query);
				
				result = prepareParticipantsDatas(datas);
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
				
		} else {
			
			System.out.println("Pas de connexion � la BDD");
			
		}
		
		return result;
		
	}
	
	@GET
	@Path("/courses")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getCourses() {
		
		String result = new String();
		
		//String query = "SELECT * FROM V_COURSES";
		
		String query = "SELECT `Course`,`Date de d�but d'inscription`, `Date de fin d'inscription`, `Date de d�roulement`, `Nombre de participants max`, `Distance entre relais` FROM `v_courses`";
		
		Connection conn = null;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection(bdd);
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			System.out.println("Driver non d�finit");
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		if ( conn != null ) {
			
			Statement statement;
			
			try {
				
				statement = conn.createStatement();
				
				ResultSet datas = statement.executeQuery(query);
				
				result = prepareCourseDatas(datas);
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
				
		} else {
			
			System.out.println("Pas de connexion � la BDD");
			
		}
		
		return result;
		
	}
	
	
	@GET
	@Path("/resultats")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getResultats() {
		
		String result = new String();
		
		String query = "SELECT * FROM RESULTATS";
		
		Connection conn = null;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection(bdd);
			
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			System.out.println("Driver non d�finit");
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		if ( conn != null ) {
			
			Statement statement;
			
			try {
				
				statement = conn.createStatement();
				
				ResultSet datas = statement.executeQuery(query);
				
				result = prepareResultatsDatas(datas);
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
				
		} else {
			
			System.out.println("Pas de connexion � la BDD");
			
		}
		
		return result;
		
	}
	
	
	private String prepareParticipantsDatas(ResultSet datas) {
		
		JSONArray participants = new JSONArray(); 
		
		try {
			while (datas.next()) {
				
				JSONObject obj = new JSONObject();
				//-R�cuperation des �l�ments de la bdd
				
				obj.put(participants_cols[0], datas.getInt(participants_cols[0]));
				
				obj.put(participants_cols[1], datas.getString(participants_cols[1]));
				
				obj.put(participants_cols[2], datas.getString(participants_cols[2]));
				
				obj.put(participants_cols[3], datas.getString(participants_cols[3]));
				
				obj.put(participants_cols[4], datas.getString(participants_cols[4]));
				
				obj.put(participants_cols[5], datas.getString(participants_cols[5]));
				
				obj.put(participants_cols[6], datas.getString(participants_cols[6]));
				
				obj.put(participants_cols[7], datas.getString(participants_cols[7]));
				
				obj.put(participants_cols[8], datas.getString(participants_cols[8]));
				
				obj.put(participants_cols[9], datas.getBoolean(participants_cols[9]));
				
				participants.add(obj);
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return participants.toJSONString();
	}
	
	private String prepareCourseDatas(ResultSet datas) {
		
		JSONArray courses = new JSONArray();
		
		try {
			while (datas.next()) {
				
				JSONObject obj = new JSONObject();
				
				JSONArray array = new JSONArray();
				
				//-R�cuperation des �l�ments de la bdd
				/*obj.put(courses_cols[0], datas.getString(courses_cols[0]));
				
				obj.put(courses_cols[1], datas.getString(courses_cols[1]));
				
				obj.put(courses_cols[2], datas.getString(courses_cols[2]));
				
				obj.put(courses_cols[3], datas.getString(courses_cols[3]));
				
				obj.put(courses_cols[4], datas.getInt(courses_cols[4]));
				
				obj.put(courses_cols[5], datas.getInt(courses_cols[5]));
				
				obj.put(courses_cols[6], datas.getString(courses_cols[6]));
				
				obj.put(courses_cols[7], datas.getInt(courses_cols[7]));
				
				obj.put(courses_cols[8], datas.getInt(courses_cols[8]));
				
				obj.put(courses_cols[9], datas.getInt(courses_cols[9]));
				*/
				
				/*array.add(datas.getString(courses_cols[0]));
				
				array.add(datas.getString(courses_cols[1]));
				
				array.add(datas.getString(courses_cols[2]));
				
				array.add(datas.getString(courses_cols[3]));*/
				
				
				array.add(datas.getString(1));
				
				array.add(datas.getString(2));
				
				array.add(datas.getString(3));
				
				array.add(datas.getString(4));
				
				array.add(datas.getInt(5));
				
				array.add(datas.getString(6));
				
				/*array.add(datas.getInt(courses_cols[4]));
				
				array.add(datas.getInt(courses_cols[5]));
				
				array.add(datas.getString(courses_cols[6]));
				
				array.add(datas.getInt(courses_cols[7]));
				
				array.add(datas.getInt(courses_cols[8]));
				
				array.add(datas.getInt(courses_cols[9]));
				
				courses.add(obj);*/
				
				courses.add(array);
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return courses.toJSONString();
		
	}
	
	private String prepareResultatsDatas(ResultSet datas) {
		
		JSONArray resultats = new JSONArray();
		
		try {
			while (datas.next()) {
				
				JSONObject obj = new JSONObject();
				
				JSONArray array = new JSONArray();
				
				//-R�cuperation des �l�ments de la bdd
				/*obj.put(resultat_cols[0], datas.getString(resultat_cols[0]));
				
				obj.put(resultat_cols[1], datas.getString(resultat_cols[1]));
				
				obj.put(resultat_cols[2], datas.getInt(resultat_cols[2]));
				
				obj.put(resultat_cols[3], datas.getString(resultat_cols[3]));
				
				obj.put(resultat_cols[4], datas.getString(resultat_cols[4]));
				
				obj.put(resultat_cols[5], datas.getString(resultat_cols[5]));
				
				obj.put(resultat_cols[6], datas.getString(resultat_cols[6]));
				
				obj.put(resultat_cols[7], datas.getString(resultat_cols[7]));
				
				obj.put(resultat_cols[8], datas.getString(resultat_cols[8]));
				
				obj.put(resultat_cols[9], datas.getString(resultat_cols[9]));
				
				obj.put(resultat_cols[10], datas.getString(resultat_cols[10]));
				
				obj.put(resultat_cols[11], datas.getInt(resultat_cols[11]));*/
				
				
				array.add(datas.getString(1));
				
				array.add(datas.getString(2));
				
				array.add(datas.getInt(3));
				
				array.add(datas.getString(4));
				
				array.add(datas.getString(5));
				
				array.add(datas.getString(6));
				
				array.add(datas.getString(7));
				
				array.add(datas.getString(8));
				
				array.add(datas.getString(9));
				
				array.add(datas.getString(10));
				
				array.add(datas.getString(11));
				
				array.add(datas.getInt(12));
				
				//resultats.add(obj);
				
				resultats.add(array);
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultats.toJSONString();
		
	}
	

}
