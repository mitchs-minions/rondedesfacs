#include "SmartCard.h"

SmardCard::SmardCard()
{
	LONG status;
	readerList = NULL;
	DWORD readerListLength = SCARD_AUTOALLOCATE;

	status = SCardEstablishContext(SCARD_SCOPE_USER,NULL,NULL,&contextHandle);
	if(status != SCARD_S_SUCCESS)
	{
		throw SmardCardException(status,"SCardEstablishContext");
	}

	status = SCardListReaders(contextHandle,NULL,(LPTSTR)&readerList,&readerListLength);
	if(status != SCARD_S_SUCCESS)
	{
		throw SmardCardException(status,"SCardListReaders");
	}
}

int*SmardCard::getCardId(LPTSTR reader)
{
	LONG status;
	DWORD activeProtocol;
	DWORD bufferLength;
	SCARD_IO_REQUEST protocolHeader;

	WCHAR displayNames[200];
	DWORD displayNamesLength;
	BYTE ATRString[32];
	DWORD ATRStringLength;
	DWORD smartCardSate;
	DWORD currentProtocol;

	BYTE dataReceived[258];
	BYTE GetCardId[] = "\xFF\x01\x00\x00\x09\x80\x06\x10\x07\x04\x0A\x10\xFF\x00";

	status = SCardConnect(contextHandle,reader,SCARD_SHARE_SHARED,SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,&connectionIdentifier,&activeProtocol);
	
	while(SCARD_S_SUCCESS != status)
	{
		status = SCardConnect(contextHandle,reader,SCARD_SHARE_SHARED,SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,&connectionIdentifier,&activeProtocol);
	}

	switch(activeProtocol)
	 {
	  case SCARD_PROTOCOL_T0:
	   protocolHeader = *SCARD_PCI_T0;
	   break;

	  case SCARD_PROTOCOL_T1:
	   protocolHeader = *SCARD_PCI_T1;
	   break;
	 }

	status = SCardBeginTransaction(connectionIdentifier);
	if(status != SCARD_S_SUCCESS)
	{
		throw SmardCardException(status,"SCardBeginTransaction");
	}


	status = SCardStatus(connectionIdentifier,displayNames,&displayNamesLength,&smartCardSate,&currentProtocol,(LPBYTE)&ATRString,&ATRStringLength);
	if(status != SCARD_S_SUCCESS)
	{
		throw SmardCardException(status,"SCardStatus");
	}

	bufferLength = sizeof(dataReceived);	
		
	status = SCardTransmit(connectionIdentifier, &protocolHeader, GetCardId, sizeof(GetCardId),NULL, dataReceived, &bufferLength);
	if(status != SCARD_S_SUCCESS)
	{
		throw SmardCardException(status,"SCardTransmit");
	}
	if(bufferLength < 9)
	{
		return NULL;
	}
	int*card_id = (int*)malloc((bufferLength-9)*sizeof(int));
	for(int i = 0;i<4;i++)
	{
		card_id[i] = dataReceived[i+7];
	}
	return card_id;
}

LPTSTR SmardCard::getReaderList()
{
	return this->readerList; 
}

SmardCard::~SmardCard()
{
	SCardFreeMemory(this->contextHandle,readerList);
}

