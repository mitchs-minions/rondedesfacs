#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

#------------------------------------------------------------
#--- CREATION DES TABLES
#------------------------------------------------------------

DROP DATABASE IF EXISTS rondedesfacs;

CREATE DATABASE rondedesfacs
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

USE rondedesfacs;

CREATE TABLE TYPE_STAFF (
  ts_id      INT(11) AUTO_INCREMENT NOT NULL,
  ts_libelle VARCHAR(40)                    ,
  PRIMARY KEY (ts_id)
  )
ENGINE = InnoDB;

CREATE TABLE STATUT_COURSE (
  sc_id      INT(11) AUTO_INCREMENT NOT NULL,
  sc_libelle VARCHAR(25)                    ,
  PRIMARY KEY (sc_id)
  )
ENGINE = InnoDB;

CREATE TABLE COURSE (
  cou_id               INT(11) AUTO_INCREMENT NOT NULL,
  cou_libelle          VARCHAR(50)                    ,
  cou_date_ins_debut   DATE                           ,
  cou_date_ins_fin     DATE                           ,
  cou_date_deroulement DATE                           ,
  cou_participants_max INT                            ,
  cou_nb_relais        INT                            ,
  cou_distance         INT                            ,
  cou_sc_id            INT                            ,
  PRIMARY KEY (cou_id)
  )
ENGINE = InnoDB;


CREATE TABLE PERSONNE (
  per_id            INT(11) AUTO_INCREMENT NOT NULL, -- PK
  per_prenom        VARCHAR(25)                    ,
  per_nom           VARCHAR(25)                    ,
  per_civilite      CHAR(5)                        ,
  per_email         VARCHAR(40)                    ,
  per_tel           VARCHAR(10)                    ,
  per_adresse1      VARCHAR(50)                    ,
  per_adresse2      VARCHAR(50)                    ,
  per_ville         VARCHAR(50)                    ,
  per_cp            VARCHAR(5)                     ,
  per_region        VARCHAR(50)                    ,
  per_mdp           CHAR(104)                      ,
  PRIMARY KEY (per_id)                             ,
  UNIQUE (per_email, per_tel)
  )
ENGINE = InnoDB;

CREATE TABLE MEMBRE (
  mem_id              INT(11) NOT NULL  , -- PK/FK hérite de Personne
  mem_guid            VARCHAR(50)       ,
  mem_etablissement   VARCHAR(50)       ,
  mem_active          BOOLEAN           , 
  PRIMARY KEY (mem_id)                  ,
  UNIQUE (mem_guid)
  )
ENGINE = InnoDB;

CREATE TABLE STAFF (
  sta_id     INT(11) NOT NULL    ,  -- PK/FK hérite de Personne
  sta_ts_id  INT(11) NOT NULL    ,  -- FK
  PRIMARY KEY (sta_id)
  )
ENGINE = InnoDB;

CREATE TABLE PARTICIPANT (
  par_cou_id      INT NOT NULL          , -- PK/FK
  par_mem_id      INT NOT NULL          , -- PK/FK
  par_num_dossard INT                   , 
  par_certificat  VARCHAR(60)           ,  -- URL
  par_inscrit     BOOLEAN DEFAULT FALSE , 
  PRIMARY KEY (par_mem_id, par_cou_id)
  )
ENGINE = InnoDB;

CREATE TABLE PASSAGE (
  pas_par_cou_id INT NOT NULL   , -- PK
  pas_par_mem_id INT NOT NULL   , -- PK
  pas_id         INT NOT NULL   , -- PK
  pas_temps      INT            , -- diff temps entre une ligne et celle d'avant en secondes 
  pas_log        TIME           , -- temps de l'insertion d'une ligne
  PRIMARY KEY (pas_id, pas_par_mem_id, pas_par_cou_id)
  )
ENGINE = InnoDB;

#------------------------------------------------------------
#--- AJOUT DES CONTRAINTES
#------------------------------------------------------------
ALTER TABLE COURSE ADD CONSTRAINT FK_COURSE_cou_sc_id FOREIGN KEY (cou_sc_id) REFERENCES STATUT_COURSE (sc_id);
ALTER TABLE MEMBRE ADD CONSTRAINT FK_MEMBRE_mem_id FOREIGN KEY (mem_id) REFERENCES PERSONNE (per_id);
ALTER TABLE STAFF ADD CONSTRAINT FK_STAFF_sta_id FOREIGN KEY (sta_id) REFERENCES PERSONNE (per_id);
ALTER TABLE STAFF ADD CONSTRAINT FK_STAFF_sta_ts_id FOREIGN KEY (sta_ts_id) REFERENCES TYPE_STAFF (ts_id);
ALTER TABLE PARTICIPANT ADD CONSTRAINT FK_PARTICIPANT_par_mem_id FOREIGN KEY (par_mem_id) REFERENCES MEMBRE (mem_id);
ALTER TABLE PARTICIPANT ADD CONSTRAINT FK_PARTICIPANT_par_cou_id FOREIGN KEY (par_cou_id) REFERENCES COURSE (cou_id);

#-- empechait l'insertion de nouveaux participants.
#--ALTER TABLE PASSAGE ADD CONSTRAINT FK_PARTICIPANT_pas_par_mem_id FOREIGN KEY (pas_par_mem_id) REFERENCES PARTICIPANT (par_mem_id);
#--ALTER TABLE PASSAGE ADD CONSTRAINT FK_PARTICIPANT_pas_par_cou_id FOREIGN KEY (pas_par_cou_id) REFERENCES PARTICIPANT (par_cou_id);

#------------------------------------------------------------
#--- AJOUT DES DONNEES DICTIONNAIRES
#------------------------------------------------------------
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (1, 'Prévue');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (2, 'Inscriptions ouvertes');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (3, 'Inscriptions fermées');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (4, 'En cours');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (5, 'Finie');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (6, 'Résultats publiés');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (7, 'Annulée');

INSERT INTO TYPE_STAFF (ts_id, ts_libelle) VALUES (1, 'Organisateur');
INSERT INTO TYPE_STAFF (ts_id, ts_libelle) VALUES (2, 'Validateur');
INSERT INTO TYPE_STAFF (ts_id, ts_libelle) VALUES (3, 'Administrateur');


#------------------------------------------------------------
#--- AJOUT DES MEMBRES
#------------------------------------------------------------
#-- mdp = password06
#-- md5 = 0caccda4dabb1f62f34f219ab821b452
INSERT INTO PERSONNE (per_id, per_prenom, per_nom, per_civilite, per_email, per_tel, per_adresse1, per_adresse2, per_ville, per_cp, per_region, per_mdp) VALUES 
(1, 'John1', 'Smith1', 'M.', 'John.smith1@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(2, 'John2', 'Smith2', 'M.', 'John.smith2@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(3, 'John3', 'Smith3', 'M.', 'John.smith3@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(4, 'John4', 'Smith4', 'M.', 'John.smith4@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(5, 'John5', 'Smith5', 'M.', 'John.smith5@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(6, 'John6', 'Smith6', 'M.', 'John.smith6@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(7, 'John7', 'Smith7', 'M.', 'John.smith7@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(8, 'John8', 'Smith8', 'M.', 'John.smith8@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(9, 'John9', 'Smith9', 'M.', 'John.smith9@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(14, 'Nicolas', 'Lemarechal', 'M.', 'nicolas.lemarechal@gmail.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452');

INSERT INTO MEMBRE (mem_id, mem_guid, mem_etablissement, mem_active) VALUES
(1, 'azerty', 'IUT de Sophia', TRUE),
(2, null, 'IUT de Sophia', TRUE),
(3, null, 'IUT de Sophia', TRUE),
(4, null, 'IUT de Sophia', TRUE),
(5, null, 'IUT de Sophia', TRUE),
(6, null, 'IUT de Sophia', TRUE),
(7, null, 'IUT de Sophia', TRUE),
(8, null, 'IUT de Sophia', TRUE),
(9, null, 'IUT de Sophia', TRUE),
(14, '0A 55 D4 4B', 'IUT de Sophia', TRUE);

#------------------------------------------------------------
#--- AJOUT DU STAFF
#------------------------------------------------------------
INSERT INTO PERSONNE (per_id, per_prenom, per_nom, per_civilite, per_email, per_tel, per_adresse1, per_adresse2, per_ville, per_cp, per_region, per_mdp) VALUES 
(10, 'Organisateur 1', '', 'M.', 'org1@outlook.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(11, 'Organisateur 2', '', 'M.', 'org2@outlook.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(12, 'Kevin', 'Simon', 'M.', 'kevin.simon@outlook.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452'),
(13, 'Alexandre', 'Breugnon', 'Dr.', 'alexandre.breugnon@doc.com', '0760745758', '70 AV Alfred Borriglione', 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', '0caccda4dabb1f62f34f219ab821b452');


INSERT INTO STAFF (sta_id, sta_ts_id) VALUES
(10, 1),
(11, 1),
(12, 3),
(13, 2);


#------------------------------------------------------------
#--- AJOUT DES COURSES
#------------------------------------------------------------
INSERT INTO COURSE (cou_id, cou_libelle, cou_date_ins_debut, cou_date_ins_fin, cou_date_deroulement, cou_participants_max, cou_nb_relais, cou_distance, cou_sc_id) VALUES
(1, 'Rondes des Facs 2013',       '2013-01-01', '2013-03-01', '2013-03-30', 300, 2, 2, 6),
(2, 'Rondes des Facs 2014',       '2014-01-01', '2014-03-01', '2014-03-30', 300, 2, 2, 7),
(3, 'Rondes des Facs 2015 Q1',    '2015-01-01', '2014-03-01', '2015-03-30', 300, 2, 2, 5),
(4, 'Rondes des Facs 2015 Q2',    '2015-01-01', '2015-04-01', SYSDATE(),    300, 2, 2, 4),
(5, 'Rondes des Facs 2015 Mudday',   '2015-01-01', '2015-04-01', '2015-09-01', 300, 2, 2, 3),
(6, 'Rondes des Facs 2015 Nice', '2015-01-01', '2015-08-01', '2015-11-01', 300, 2, 2, 2),
(7, 'Rondes des Facs 2016',       '2016-01-01', '2016-04-01', '2016-03-30', 300, 2, 2, 1);


#------------------------------------------------------------
#--- AJOUT DES PARTICIPANTS
#------------------------------------------------------------

INSERT INTO PARTICIPANT(par_mem_id, par_cou_id, par_num_dossard, par_certificat, par_inscrit) VALUES
(1, 1, 1, NULL, TRUE),
(2, 1, 2, NULL, TRUE),
(3, 1, 3, NULL, TRUE),
(4, 1, 4, NULL, TRUE),
(5, 1, 5, NULL, TRUE),
(6, 1, 6, NULL, TRUE),
(7, 1, 7, NULL, TRUE),
(8, 1, 8, NULL, TRUE),
(9, 1, 9, NULL, TRUE),

(1, 2, 1, NULL, TRUE),
(2, 2, 2, NULL, TRUE),
(3, 2, 3, NULL, TRUE),
(4, 2, 4, NULL, TRUE),
(5, 2, 5, NULL, TRUE),
(6, 2, 6, NULL, TRUE),
(7, 2, 7, NULL, TRUE),
(8, 2, 8, NULL, TRUE),
(9, 2, 9, NULL, TRUE),

(1, 3, 1, NULL, TRUE),
(2, 3, 2, NULL, TRUE),
(3, 3, 3, NULL, TRUE),
(4, 3, 4, NULL, TRUE),
(5, 3, 5, NULL, TRUE),
(6, 3, 6, NULL, TRUE),
(7, 3, 7, NULL, TRUE),
(8, 3, 8, NULL, TRUE),
(9, 3, 9, NULL, TRUE),

(1, 4, 1, NULL, TRUE),
(2, 4, 2, NULL, TRUE),
(3, 4, 3, NULL, TRUE),
(4, 4, 4, NULL, TRUE),
(5, 4, 5, NULL, TRUE),
(6, 4, 6, NULL, TRUE),
(7, 4, 7, NULL, TRUE),
(8, 4, 8, NULL, TRUE),
(9, 4, 9, NULL, TRUE),

(1, 5, 1, NULL, TRUE),
(2, 5, 2, NULL, TRUE),
(3, 5, 3, NULL, TRUE),
(4, 5, 4, NULL, TRUE),
(5, 5, 5, NULL, TRUE),
(6, 5, 6, NULL, TRUE),
(7, 5, 7, NULL, TRUE),
(8, 5, 8, NULL, TRUE),
(9, 5, 9, NULL, TRUE),

(1, 6, 1, NULL, TRUE),
(2, 6, 2, NULL, TRUE),
(3, 6, 3, NULL, TRUE),
(4, 6, 4, NULL, TRUE),
(5, 6, 5, NULL, TRUE),
(6, 6, 6, NULL, TRUE),
(7, 6, 7, NULL, TRUE),
(8, 6, 8, NULL, TRUE),
(9, 6, 9, NULL, TRUE),

(1, 7, 1, NULL, FALSE),
(2, 7, 2, NULL, FALSE),
(3, 7, 3, NULL, FALSE),
(4, 7, 4, NULL, FALSE),
(5, 7, 5, NULL, FALSE),
(6, 7, 6, NULL, FALSE),
(7, 7, 7, NULL, FALSE),
(8, 7, 8, NULL, FALSE),
(9, 7, 9, NULL, FALSE);

#------------------------------------------------------------
#--- AJOUT DES PASSAGES
#------------------------------------------------------------
 
INSERT INTO PASSAGE(pas_id, pas_par_mem_id, pas_par_cou_id, pas_temps, pas_log) VALUES
(1, 1, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 1, 1, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 1, 1, TIME_TO_SEC(TIMEDIFF('10:20:00', '10:10:00')), '10:20:00'),
(4, 1, 1, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:20:00')), '10:30:00'),
(5, 1, 1, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),
(6, 1, 1, TIME_TO_SEC(TIMEDIFF('10:50:00', '10:40:00')), '10:50:00'),
(7, 1, 1, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:50:00')), '11:00:00'),

(1, 2, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 2, 1, TIME_TO_SEC(TIMEDIFF('10:15:00', '10:00:00')), '10:15:00'),
(3, 2, 1, TIME_TO_SEC(TIMEDIFF('10:25:00', '10:15:00')), '10:25:00'),
(4, 2, 1, TIME_TO_SEC(TIMEDIFF('10:35:00', '10:25:00')), '10:35:00'),
(5, 2, 1, TIME_TO_SEC(TIMEDIFF('10:45:00', '10:35:00')), '10:45:00'),
(6, 2, 1, TIME_TO_SEC(TIMEDIFF('10:59:50', '10:45:00')), '10:59:50'),

(1, 3, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 3, 1, TIME_TO_SEC(TIMEDIFF('10:25:00', '10:00:00')), '10:25:00'),
(3, 3, 1, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:25:00')), '10:40:00'),

(1, 4, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 4, 1, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:00:00')), '10:30:00'),
(3, 4, 1, TIME_TO_SEC(TIMEDIFF('10:55:00', '10:30:00')), '10:55:00'),

(1, 5, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 5, 1, TIME_TO_SEC(TIMEDIFF('10:15:00', '10:00:00')), '10:15:00'),
(3, 5, 1, TIME_TO_SEC(TIMEDIFF('10:20:00', '10:15:00')), '10:20:00'),
(4, 5, 1, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:20:00')), '10:30:00'),
(5, 5, 1, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),
(6, 5, 1, TIME_TO_SEC(TIMEDIFF('10:50:00', '10:40:00')), '10:50:00'),
(7, 5, 1, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:50:00')), '11:00:00'),

(1, 6, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 6, 1, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 6, 1, TIME_TO_SEC(TIMEDIFF('10:20:00', '10:10:00')), '10:20:00'),
(4, 6, 1, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:20:00')), '10:30:00'),
(5, 6, 1, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),

(1, 7, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 7, 1, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 7, 1, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:10:00')), '10:30:00'),
(4, 7, 1, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),
(5, 7, 1, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:40:00')), '11:00:00'),

(1, 8, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 8, 1, TIME_TO_SEC(TIMEDIFF('10:20:00', '10:00:00')), '10:20:00'),
(3, 8, 1, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:20:00')), '10:30:00'),
(4, 8, 1, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),
(5, 8, 1, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:40:00')), '11:00:00'),

(1, 9, 1, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 9, 1, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 9, 1, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:10:00')), '10:30:00'),
(4, 9, 1, TIME_TO_SEC(TIMEDIFF('10:50:00', '10:30:00')), '10:50:00'),
(5, 9, 1, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:50:00')), '11:00:00'),

(1, 1, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 1, 2, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 1, 2, TIME_TO_SEC(TIMEDIFF('10:20:00', '10:10:00')), '10:20:00'),
(4, 1, 2, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:20:00')), '10:30:00'),
(5, 1, 2, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),
(6, 1, 2, TIME_TO_SEC(TIMEDIFF('10:50:00', '10:40:00')), '10:50:00'),
(7, 1, 2, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:50:00')), '11:00:00'),

(1, 2, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 2, 2, TIME_TO_SEC(TIMEDIFF('10:15:00', '10:00:00')), '10:15:00'),
(3, 2, 2, TIME_TO_SEC(TIMEDIFF('10:25:00', '10:15:00')), '10:25:00'),
(4, 2, 2, TIME_TO_SEC(TIMEDIFF('10:35:00', '10:25:00')), '10:35:00'),
(5, 2, 2, TIME_TO_SEC(TIMEDIFF('10:45:00', '10:35:00')), '10:45:00'),
(6, 2, 2, TIME_TO_SEC(TIMEDIFF('10:55:00', '10:45:00')), '10:55:00'),
(7, 2, 2, TIME_TO_SEC(TIMEDIFF('11:05:00', '10:55:00')), '11:05:00'),

(1, 3, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 3, 2, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:00:00')), '10:40:00'),

(1, 4, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 4, 2, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 4, 2, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:10:00')), '10:30:00'),
(4, 4, 2, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),

(1, 5, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 5, 2, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:00:00')), '10:30:00'),
(3, 5, 2, TIME_TO_SEC(TIMEDIFF('10:40:00', '10:30:00')), '10:40:00'),
(4, 5, 2, TIME_TO_SEC(TIMEDIFF('10:50:00', '10:40:00')), '10:50:00'),

(1, 6, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 6, 2, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 6, 2, TIME_TO_SEC(TIMEDIFF('10:28:00', '10:10:00')), '10:28:00'),
(4, 6, 2, TIME_TO_SEC(TIMEDIFF('10:49:00', '10:28:00')), '10:49:00'),

(1, 7, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 7, 2, TIME_TO_SEC(TIMEDIFF('10:17:00', '10:00:00')), '10:17:00'),
(3, 7, 2, TIME_TO_SEC(TIMEDIFF('10:26:00', '10:17:00')), '10:26:00'),
(4, 7, 2, TIME_TO_SEC(TIMEDIFF('10:31:00', '10:26:00')), '10:31:00'),
(5, 7, 2, TIME_TO_SEC(TIMEDIFF('10:43:00', '10:31:00')), '10:43:00'),
(6, 7, 2, TIME_TO_SEC(TIMEDIFF('10:58:00', '10:43:00')), '10:58:00'),

(1, 8, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 8, 2, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 8, 2, TIME_TO_SEC(TIMEDIFF('10:20:00', '10:10:00')), '10:20:00'),
(4, 8, 2, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:20:00')), '10:30:00'),
(5, 8, 2, TIME_TO_SEC(TIMEDIFF('10:50:00', '10:30:00')), '10:50:00'),
(6, 8, 2, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:50:00')), '11:00:00'),

(1, 9, 2, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 9, 2, TIME_TO_SEC(TIMEDIFF('10:10:00', '10:00:00')), '10:10:00'),
(3, 9, 2, TIME_TO_SEC(TIMEDIFF('10:30:00', '10:10:00')), '10:30:00'),
(4, 9, 2, TIME_TO_SEC(TIMEDIFF('10:50:00', '10:30:00')), '10:50:00'),
(5, 9, 2, TIME_TO_SEC(TIMEDIFF('11:00:00', '10:50:00')), '11:00:00'),

(1, 1, 3, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:00:00'),
(2, 1, 3, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:10:00'),
(3, 1, 3, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:30:00'),
(4, 1, 3, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '10:50:00'),
(5, 1, 3, TIME_TO_SEC(TIMEDIFF('10:00:00', '10:00:00')), '11:00:00');

#------------------------------------------------------------
#--- AJOUT DE LAVUE POUR LES RESULTATS
#------------------------------------------------------------

CREATE OR REPLACE VIEW V_PARTICIPANTS AS 
SELECT
  par_num_dossard             AS 'N° dossard'           , 
  per_prenom                  AS 'Prénom'               , 
  per_nom                     AS 'Nom'                  , 
  per_civilite                AS 'Civilité'             , 
  per_ville                   AS 'Ville'                , 
  per_cp                      AS 'Code Postal'          , 
  per_region                  AS 'Région'               , 
  mem_etablissement           AS 'Etablissement'        , 
  cou_libelle                 AS 'Course'               ,
  par_inscrit                 AS 'Inscription validée'
FROM PERSONNE, MEMBRE, PARTICIPANT, COURSE
WHERE per_id = mem_id
AND   par_mem_id = mem_id
AND par_cou_id = cou_id;

CREATE OR REPLACE VIEW V_COURSES AS 
SELECT
  cou_libelle                 AS 'Course'                                   , 
  cou_date_ins_debut          AS 'Date de début d''inscription'             , 
  cou_date_ins_fin            AS 'Date de fin d''inscription'               , 
  cou_date_deroulement        AS 'Date de déroulement'                      , 
  cou_nb_relais               AS 'Nombre de relais'                         , 
  cou_distance                AS 'Distance entre relais'                    , 
  sc_libelle                  AS 'Statut'                                   , 
  Count(instr(par_inscrit, 1) > 0)   AS 'Nombre de participants (broken)'   , 
  cou_participants_max        AS 'Nombre de participants max'               , 
  Count(par_cou_id)           AS 'Nombre d''inscrit'
FROM PARTICIPANT, COURSE, STATUT_COURSE
WHERE par_cou_id = cou_id
AND cou_sc_id = sc_id
GROUP BY cou_id;

CREATE TABLE RESULTATS AS 
(SELECT
  cou_libelle                 AS 'course'               ,
                                 'rang'                 ,
  Count(pas_id)               AS 'nb_passages'          ,
  SEC_TO_TIME(SUM(pas_temps)) AS 'temps'                ,
  per_prenom                  AS 'prenom'               ,
  per_nom                     AS 'nom'                  ,
  per_civilite                AS 'civilite'             ,
  per_ville                   AS 'ville'                ,
  per_cp                      AS 'cp'                   ,
  per_region                  AS 'region'               ,
  mem_etablissement           AS 'etablissement'        ,
  par_num_dossard             AS 'num_dossard'
  
FROM  PERSONNE, MEMBRE, PARTICIPANT, COURSE, PASSAGE
WHERE per_id = mem_id
AND   par_mem_id = mem_id
AND   par_cou_id = cou_id
AND   pas_par_mem_id = par_mem_id
AND   pas_par_cou_id = par_cou_id
GROUP BY pas_par_mem_id, pas_par_cou_id
ORDER BY course ASC, rang ASC, nb_passages DESC, temps ASC);



#------------------------------------------------------------
#--- AJOUT D'UN UTILISATEUR admin (mdp : admin)
#------------------------------------------------------------
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' IDENTIFIED BY PASSWORD '*4ACFE3202A5FF5CF467898FC58AAB1D615029441' WITH GRANT OPTION;

#------------------------------------------------------------
#--- AJOUT DES TRIGGERS
#------------------------------------------------------------
/**
Attribut le numéro de dossard au nouveau participant de la course.
*/
DELIMITER //
CREATE TRIGGER insb_participant BEFORE INSERT ON PARTICIPANT FOR EACH ROW
BEGIN
  DECLARE v_num_dossard INTEGER;
  SELECT MAX(par_num_dossard) INTO v_num_dossard FROM PARTICIPANT WHERE par_cou_id = NEW.par_cou_id;

  IF v_num_dossard IS NULL THEN
    SET NEW.par_num_dossard = 1;
  ELSE
    SET NEW.par_num_dossard = v_num_dossard + 1;
  END IF;
END//
DELIMITER ;

#------------------------------------------------------------
#--- AJOUT DES PROCEDURES STOCKEES
#------------------------------------------------------------

DELIMITER $$
/**
* Ajoute un nouveau passage pour la participant ayant le guid passé en paramètre.
* A utiliser pendant la course.
*/
CREATE PROCEDURE ajouter_passage(p_guid VARCHAR(20), p_cou_id INTEGER, p_num_relais INTEGER) 
BEGIN
  DECLARE v_passage_id INTEGER;
  DECLARE v_membre_id  INTEGER;

  -- si le participant existe bien alors l'insertion se fait.
  IF p_guid IS NOT NULL AND p_cou_id IS NOT NULL AND p_num_relais IS NOT NULL THEN
    SELECT mem_id INTO v_membre_id FROM MEMBRE WHERE mem_guid = p_guid;

    IF v_membre_id IS NOT NULL THEN

      SELECT Max(pas_id) INTO v_passage_id FROM PASSAGE WHERE pas_par_cou_id = p_cou_id AND pas_par_mem_id = v_membre_id;

      -- si le participant n'a pas encore de passages alors l'id du passage est déterminé par le numéro de relais.
      IF v_passage_id IS NULL THEN
        IF p_num_relais = 1 THEN
          SET v_passage_id = 1;
        ELSEIF p_num_relais = 2 THEN
          SET v_passage_id = 2;
        END IF;
      ELSE
        SET v_passage_id = v_passage_id + 2;
      END IF;
      
      INSERT INTO PASSAGE(pas_id, pas_par_mem_id, pas_par_cou_id, pas_temps, pas_log) VALUES (v_passage_id, v_membre_id, p_cou_id, 0, Now());
    END IF;
  END IF;
END$$

/**
* Ajoute un nouveau passage pour la participant ayant le guid passé en paramètre.
* A utiliser pour tester.
*/
CREATE PROCEDURE ajouter_passage_bis(p_guid VARCHAR(20), p_cou_id INTEGER, p_num_relais INTEGER) 
BEGIN
  DECLARE v_passage_id INTEGER;
  DECLARE v_membre_id  INTEGER;

  -- si le participant existe bien alors l'insertion se fait.
  IF p_guid IS NOT NULL AND p_cou_id IS NOT NULL AND p_num_relais IS NOT NULL THEN
    SELECT mem_id INTO v_membre_id FROM MEMBRE WHERE mem_guid = p_guid;
    SELECT Max(pas_id) INTO v_passage_id FROM PASSAGE WHERE pas_par_cou_id = p_cou_id AND pas_par_mem_id = v_membre_id;

    -- si le participant n'a pas encore de passages alors l'id du passage est déterminé par le numéro de relais.
    IF v_passage_id IS NULL THEN
      SET v_passage_id = 1;
    ELSE
      SET v_passage_id = v_passage_id + 1;
    END IF;
    
    INSERT INTO PASSAGE(pas_id, pas_par_mem_id, pas_par_cou_id, pas_temps, pas_log) VALUES (v_passage_id, v_membre_id, p_cou_id, 0, Now());
  END IF;
END$$

/**
 * Met à jour le temps de chaque passages qui n'en avaient pas.
 */
CREATE PROCEDURE calculer_resultats()
BEGIN
  

  -- variable loop pas_loop
  DECLARE done INT DEFAULT 0;

  -- variables de remplissage pour le curseur
  DECLARE v_cou_id   INT;
  DECLARE v_mem_id   INT;
  DECLARE v_pas_id   INT;
  DECLARE v_temps    INT;
  DECLARE v_log      TIME;

  -- variable pour récuppèrer le log précédent du passage
  DECLARE v_log_prec TIME;
  
  -- recuppère tous les passages
  DECLARE pas_cursor CURSOR FOR
    SELECT   pas_par_cou_id, pas_par_mem_id, pas_id, pas_temps, pas_log 
    FROM     Passage
    ORDER BY pas_par_cou_id, pas_par_mem_id, pas_id;

  -- variables utiles pour le curseur
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  -- Pour chaque passage de chaque coureur de chaque course 
  -- on calcule le temps de passage en fonction du log précdent et de celui actuel
  
  DROP TABLE RESULTATS;
  CREATE TABLE RESULTATS AS 
  (SELECT
    cou_libelle                 AS 'course'               ,
                                   'rang'                 ,
    Count(pas_id)               AS 'nb_passages'          ,
    SEC_TO_TIME(SUM(pas_temps)) AS 'temps'                ,
    per_prenom                  AS 'prenom'               ,
    per_nom                     AS 'nom'                  ,
    per_civilite                AS 'civilite'             ,
    per_ville                   AS 'ville'                ,
    per_cp                      AS 'cp'                   ,
    per_region                  AS 'region'               ,
    mem_etablissement           AS 'etablissement'        ,
    par_num_dossard             AS 'num_dossard'
    
  FROM  PERSONNE, MEMBRE, PARTICIPANT, COURSE, PASSAGE
  WHERE per_id = mem_id
  AND   par_mem_id = mem_id
  AND   par_cou_id = cou_id
  AND   pas_par_mem_id = par_mem_id
  AND   pas_par_cou_id = par_cou_id
  GROUP BY pas_par_mem_id, pas_par_cou_id
  ORDER BY course ASC, rang ASC, nb_passages DESC, temps ASC);

  OPEN pas_cursor;
  pas_loop: LOOP
    
    FETCH pas_cursor INTO v_cou_id, v_mem_id, v_pas_id, v_temps, v_log;
    
    IF done THEN
      LEAVE pas_loop;
    END IF;

    -- Si c'est un passage qui n'a pas de temps Alors le traitement commence
    
    IF (v_pas_id > 1 AND v_temps = 0) THEN
      
      -- récuppération du log du passage précédent
      SELECT pas_log INTO v_log_prec FROM Passage
        WHERE pas_par_cou_id = v_cou_id
        AND   pas_par_mem_id = v_mem_id
        AND   pas_id         = v_pas_id - 1;

      -- calcul du temps pour ce passage
      SET v_temps = TIME_TO_SEC(TIMEDIFF(v_log, v_log_prec));

      -- maj du temps pour ce passage
      UPDATE Passage 
        SET pas_temps = v_temps 
        WHERE pas_par_cou_id = v_cou_id
        AND   pas_par_mem_id = v_mem_id
        AND   pas_id         = v_pas_id;
    END IF;
    
  END LOOP pas_loop;
  CLOSE pas_cursor;
END$$

/**
 * 
 */
CREATE PROCEDURE calculer_rangs()
BEGIN
  
  -- variable loop pas_loop
  DECLARE done INT DEFAULT 0;

  -- variables de remplissage pour le curseur
  DECLARE v_course       VARCHAR(50);
  DECLARE v_num_dossard  INT;
  

  -- variables de traitements
  DECLARE v_course_prec  VARCHAR(50);
  DECLARE v_rang         INT;

  -- recuppère tous les passages
  DECLARE res_cursor CURSOR FOR
    SELECT course, num_dossard FROM RESULTATS ORDER BY course ASC, rang ASC, nb_passages DESC, temps ASC;

  -- variables utiles pour le curseur
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  SET v_course_prec = 'xxx';

  -- Pour chaque course et chaque participant on calcule le rang
  OPEN res_cursor;
  res_loop: LOOP
    
    FETCH res_cursor INTO v_course, v_num_dossard;
    
    IF done THEN
      LEAVE res_loop;
    END IF;

    -- Si c'est plus la même course alors le rang = 1
    -- Sinon il s'inscrémente
    IF v_course <> v_course_prec THEN
      SET v_course_prec = v_course;
      SET v_rang = 1;
    ELSE
      SET v_rang = v_rang + 1;
    END IF;

    UPDATE RESULTATS SET rang = v_rang WHERE course = v_course AND num_dossard = v_num_dossard;
    
  END LOOP res_loop;
  CLOSE res_cursor;
END$$

CREATE PROCEDURE maj_res()
BEGIN
  call calculer_resultats();
  call calculer_rangs();
END$$


/**
 * 
 */
CREATE PROCEDURE appareiller_carte(p_guid VARCHAR(20), p_mem_id INTEGER)
BEGIN
  UPDATE MEMBRE SET mem_guid = p_guid WHERE mem_id = p_mem_id;
END$$
DELIMITER ;





/*


call ajouter_passage('azerty', 3, 2);
call calculer_resultats();

SELECT par_cou_id, par_mem_id, par_num_dossard
FROM Participant
WHERE par_cou_id = 3
ORDER BY  par_cou_id, par_mem_id, par_num_dossard


#-- TODO LIST
#-- Résoudre le nombre de participants
#-- prendre que les passages valides.
 */
