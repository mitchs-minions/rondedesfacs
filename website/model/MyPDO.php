<?php
namespace model;
class MyPDO
{
    /**
    * Instance de la classe PDO
    *
    * @var PDO
    * @access private
    */ 
    private $PDOInstance = null;

    /**
    * Instance de la classe MyPDo
    *
    * @var SPDO
    * @access private
    * @static
    */ 
    private static $instance = null;


    /**
    * Constructeur
    *
    * @param void
    * @return void
    * @see PDO::__construct()
    * @access private
    */
    private function __construct()
    {
        // recupération des données provenant du fichier de config
        $fichier = file_get_contents('../common/db.ini');
        $infoDns = json_decode($fichier, true);
        $dns = $infoDns['dbType'].':host='.$infoDns['dbHost'].';dbname='.$infoDns['dbName'];
        $utilisateur = $infoDns['userLogin'];
        $motDePasse = $infoDns['userPassword'];

        // création d'une instance de pdo
        $this->PDOInstance = new PDO( $dns, $utilisateur, $motDePasse ); 
        $this->PDOInstance->query("SET NAMES UTF8");
        $this->PDOInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
    * Crée et retourne l'objet MyPDO
    *
    * @access public
    * @static
    * @param void
    * @return SPDO $instance
    */
    public static function getInstance()
    {  
        /*if(is_null(self::$PDOInstance))
        {
            self::$instance = new MyPDO();
        }
*/
        if (!isset(self::$PDOInstance)) {
          $c = __CLASS__;
          $instance = new $c;
      }
      return self::$PDOInstance;
  }

  public static function closeConnection()
  {
    self::$PDOInstance = null;
}
}