<?php

session_start();
include '../controllers/config_init.php';

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Espace - Configuration</title>

        <meta name="author" content="">
        <meta name="description" content="Ronde des facs">
        <meta name="geo.placename" content="Nice, PACA, Cote-d'Azur,France">
        <meta name="keywords" content="Ronde des facs,Etudiant,Université Sophia Antipolis,Course,4km">
        <meta name="copyright" content="">
        <meta name="generator" content="PHPStorm, Sublime Text">
        <meta name="robots" content="all">

        <link rel="shortcut icon" title="Image" type="image/png" href="../web/img/">
        <link rel="image_src" href="../web/img/">
        <link rel="stylesheet" href="../web/css/espaceAdmin.css">
        <!--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css">-->
        <script type="text/javascript" src="../web/js/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready( function() {
                /* Menu à afficher */
                $('.deroule_menu').click(function(){
                    $(this).next().toggleClass('display_block');
                });
            });
        </script>
    </head>
    <body>
        <?php if (isset($_SESSION["email"]) && isset($_SESSION["password"]) && isset($_SESSION["libelle"])) { ?>
        <div class='main'>
            <div class='header'>
                <div class='title'>Espace <?php echo $_SESSION["libelle"]; ?></div>
                <div class='user'>
                    <div class='name'>
                        <a href="#" title="Mon profile"><?php echo $_SESSION["nom"]." ".$_SESSION["prenom"]; ?></a>
                    </div>
                    <div class='logout'>
                        <a href="../controllers/fonction.php?deco" title="Déconnecter">Déconnecter</a>
                    </div>
                </div>
            </div>
            <div class='menu'>
                <div class='nav'>
                    <div class='search'>
                        <input placeholder='Rechercher' type='text'>
                    </div>
                    <ul id='menu'>
                        <?php

                        switch ($_SESSION["libelle"]) {
                            case 'Organisateur':
                                echo "<li>
                                    <a class='deroule_menu'>Tableau de bord</a>
                                    <ul class='menu_cacher'>
                                        <li><a href='resultats.php'>Consultation résultats</a></li>
                                        <li><a href=''>Valider certificat</a></li>
                                        <li><a href='appareillerBadge.php'>Appareiller</a></li>
                                        <li><a href=''>Valider relais</a></li>
                                        <li><a href='extraireBDD.php'>Extraire</a></li>
                                    </ul>
                                </li>";
                                break;

                            case 'Validateur':
                                echo "<li>
                                    <a class='deroule_menu'>Tableau de bord</a>
                                    <ul class='menu_cacher'>
                                        <li><a href=''>Valider certificat</a></li>
                                        <li><a href=''>Valider relais</a></li>
                                    </ul>
                                </li>";
                                break;

                            case 'Administrateur':
                                echo "<li>
                                    <a class='deroule_menu'>Tableau de bord</a>
                                    <ul class='menu_cacher'>
                                        <li><a href='resultats.php'>Consultation résultats</a></li>
                                        <li><a href=''>Valider certificat</a></li>
                                        <li><a href='appareillerBadge.php'>Appareiller</a></li>
                                        <li><a href=''>Valider relais</a></li>
                                        <li><a href='extraireBDD.php'>Extraire</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class='deroule_menu'>Configuration</a>
                                    <ul class='menu_cacher'>
                                        <li><a href=''>Gérer membres</a></li>
                                        <li><a href=''>Inscription participant</a></li>
                                        <li><a href=''>Gérer participants</a></li>
                                    </ul>
                                </li>";
                                break;
                        }

                        ?>
                    </ul>
                </div>
                <div class='content'>
                    <div class='title'>
                        <a href="#" style="float: right;"></a>
                    </div>
                    <div class='grid' id='a1'>
                        <div class='col'>
                            <form method="POST" action="">
                                <legend> Sélection de la course</legend>
                                <label> Course</label>
                                <?php

                                $requete = "SELECT * FROM `COURSE`";
                                $sql = $connexion->query($requete);

                                ?> <select name='course' id='course'> <?php

                                    while ($data = $sql->fetch(PDO::FETCH_ASSOC)) {

                                        echo "<option value='$data[cou_id]'>$data[cou_libelle]</option>";
                                    }?>
                                    
                                </select>
                                <legend> Sélection du participant</legend>
                                <label> Personne</label>
                                <?php

                                $requete = "SELECT * FROM `PARTICIPANT`, `PERSONNE` WHERE `par_mem_id` = `per_id` GROUP BY `par_mem_id`";
                                $sql = $connexion->query($requete);

                                ?> <select name='participant' id='participant'> <?php

                                    while ($data = $sql->fetch(PDO::FETCH_ASSOC)) {

                                        echo "<option value='$data[per_id]'>$data[per_prenom] $data[per_nom]</option>";
                                    }?>
                                    
                                </select>
                                <div class='btnset'>
                                    <input class="btn pri" type="submit" name="appareiller" value="Appareiller" />
                                </div>
                            </form>

                            <?php

                            if (isset($_POST) && isset($_POST["appareiller"])) {


                                echo "<div class='grid' id='a1'>
                                        <div class='col'>
                                            Appareillage Effectuer
                                        </div>
                                    </div>";

                                exec ( 'C:\Users\Public\SmartCardRDF.exe', 'C:\Users\Public\TestProcedureStockéée.exe', '2', $_POST["participant"] );

                            }

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } else { header("Location: ../admin.php"); } ?>
    </body>
</html>

numéro de course et membre
