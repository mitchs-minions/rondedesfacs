<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 18/04/2015
 * Time: 12:10
 */

namespace Model;

/**
 * Class StatutCourse
 * @package Model
 */

include_once('connection.php');

class StatutCourse
{
    //-------------------------------------------------------------------------
    //---	PROPRIETES
    //-------------------------------------------------------------------------
     var $_id;
     var $_libelle;

    //-------------------------------------------------------------------------
    //---	CONSTRUCTEURS
    //-------------------------------------------------------------------------
    public function __construct($id)
    {
        // RECUPERATION DES DONNEES DE LA BD
        //$sql = $connexion->query("select sc_id, sc_libelle from statut_course where sc_id = :sc_id");
        //$connexion->bindParameter(":sc_id" => $id);
        $results = array();
        // VERFICATION
        if (is_null($results) == true){
            throw new \Exception();
        }
        else
        {
            SetId($id);
            SetLibelle($results["sc_libelle"]);
        }
    }


    //-------------------------------------------------------------------------
    //---	GETTERS
    //-------------------------------------------------------------------------
    public function GetId() {
        return $this->_id;
    }
    public function GetLibelle() {
        return $this->_libelle;
    }

    //-------------------------------------------------------------------------
    //---	SETTERS
    //-------------------------------------------------------------------------
    public function SetId($id) {
        $this->_id = $id;
    }
    public function SetLibelle($libelle) {
        $this->_libelle = $libelle;
    }

}

