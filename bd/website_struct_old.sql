#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

#------------------------------------------------------------
#--- CREATION DES TABLES
#------------------------------------------------------------
DROP DATABASE IF EXISTS rondedesfacs;
CREATE DATABASE rondedesfacs;
USE rondedesfacs;

CREATE TABLE COURSE (
  cou_id               INT(11) AUTO_INCREMENT NOT NULL,
  cou_libelle          VARCHAR(50),
  cou_date_ins_debut   DATE,
  cou_date_ins_fin     DATE,
  cou_date_deroulement DATE,
  cou_participants_max INT,
  cou_nb_relais        INT,
  cou_distance         INT,
  cou_sc_id            INT,
  PRIMARY KEY (cou_id)
)
  ENGINE = InnoDB;


CREATE TABLE PERSONNE (
  per_id            INT(11) AUTO_INCREMENT NOT NULL,
  per_guid          VARCHAR(25),
  per_prenom        VARCHAR(25),
  per_nom           VARCHAR(25),
  per_sexe          CHAR(1),
  per_email         VARCHAR(40),
  per_tel           VARCHAR(10),
  per_adresse1      VARCHAR(50),
  per_adresse2      VARCHAR(50),
  per_ville         VARCHAR(50),
  per_cp            VARCHAR(5),
  per_region        VARCHAR(50),
  per_etablissement VARCHAR(50),
  per_mdp           CHAR(104),
  PRIMARY KEY (per_id),
  UNIQUE (per_email) ,
  UNIQUE (per_guid)
)
  ENGINE = InnoDB;


CREATE TABLE TYPE_STAFF (
  ts_id      INT(11) AUTO_INCREMENT NOT NULL,
  ts_libelle VARCHAR(40),
  PRIMARY KEY (ts_id)
)
  ENGINE = InnoDB;


CREATE TABLE PASSAGE (
  pas_id         INT NOT NULL, -- PK
  pas_par_per_id INT NOT NULL, -- PK/FK
  pas_par_cou_id INT NOT NULL, -- PK/FK
  pas_num_relais INT,
  pas_temps      TIME,
  PRIMARY KEY (pas_id, pas_par_per_id, pas_par_cou_id)
)
  ENGINE = InnoDB;


CREATE TABLE STATUT_COURSE (
  sc_id      INT(11) AUTO_INCREMENT NOT NULL,
  sc_libelle VARCHAR(25),
  PRIMARY KEY (sc_id)
)
  ENGINE = InnoDB;


CREATE TABLE STAFF (
  sta_id     INT(11) AUTO_INCREMENT NOT NULL,
  sta_prenom VARCHAR(25),
  sta_nom    VARCHAR(25),
  sta_email  VARCHAR(40),
  sta_tel    VARCHAR(10),
  sta_mdp    CHAR(60),
  sta_ts_id  INT                    NOT NULL,
  PRIMARY KEY (sta_id),
  UNIQUE (sta_email)
)
  ENGINE = InnoDB;


CREATE TABLE PARTICIPANT (
  par_per_id      INT NOT NULL, -- PK/FK
  par_cou_id      INT NOT NULL, -- PK/FK
  par_num_dossard INT NOT NULL,
  par_certificat  BLOB,
  par_inscrit     BOOL,
  PRIMARY KEY (par_per_id, par_cou_id)
)
  ENGINE = InnoDB;

#------------------------------------------------------------
#--- AJOUT DES CONTRAINTES
#------------------------------------------------------------
ALTER TABLE COURSE ADD CONSTRAINT FK_COURSE_cou_sc_id FOREIGN KEY (cou_sc_id) REFERENCES STATUT_COURSE (sc_id);
ALTER TABLE STAFF ADD CONSTRAINT FK_STAFF_sta_ts_id FOREIGN KEY (sta_ts_id) REFERENCES TYPE_STAFF (ts_id);
ALTER TABLE PARTICIPANT ADD CONSTRAINT FK_PARTICIPANT_par_per_id FOREIGN KEY (par_per_id) REFERENCES PERSONNE (per_id);
ALTER TABLE PARTICIPANT ADD CONSTRAINT FK_PARTICIPANT_par_cou_id FOREIGN KEY (par_cou_id) REFERENCES COURSE (cou_id);
ALTER TABLE PASSAGE ADD CONSTRAINT FK_PARTICIPANT_pas_par_per_id FOREIGN KEY (pas_par_per_id) REFERENCES PARTICIPANT (par_per_id);
ALTER TABLE PASSAGE ADD CONSTRAINT FK_PARTICIPANT_pas_par_cou_id FOREIGN KEY (pas_par_cou_id) REFERENCES PARTICIPANT (par_cou_id);

#------------------------------------------------------------
#--- AJOUT DES DONNEES
#------------------------------------------------------------

#-- TABLES DICTIONNAIRES
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (1, 'Prévue');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (2, 'Inscriptions ouvertes');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (3, 'Inscriptions fermées');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (4, 'En cours');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (5, 'Finie');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (6, 'Résultats publiés');
INSERT INTO STATUT_COURSE (sc_id, sc_libelle) VALUES (7, 'Annulée');

INSERT INTO TYPE_STAFF (ts_id, ts_libelle) VALUES (1, 'Organisateur');
INSERT INTO TYPE_STAFF (ts_id, ts_libelle) VALUES (2, 'Validateur');
INSERT INTO TYPE_STAFF (ts_id, ts_libelle) VALUES (3, 'Administrateur');


#-- PERSONNE avec comme mdp userpw
INSERT INTO PERSONNE (per_id, per_guid, per_prenom, per_nom, per_sexe, per_email, per_tel, per_adresse1,
                      per_adresse2, per_ville, per_cp, per_region, per_etablissement, per_mdp)
VALUES (1, NULL, 'Kevin', 'Simon', 'M', 'kevin.simon@outlook.com', '0760745758', '70 AV Alfred Borriglione'
  , 'Le Vercors 2', 'Nice', '06200', 'Alpes Maritimes', 'IUT de Sophia',
        'YTRjNjE3OThmNzNkM2Y2MTliZTFiYjNid033e22ae348aeb5660fc2140aec35850c4da997OTcwMzUyZTgzNGFiYWNkZGI5NTYwZWU5');

INSERT INTO `PERSONNE` (`per_id`, `per_guid`, `per_prenom`, `per_nom`, `per_sexe`, `per_email`, `per_tel`, `per_adresse1`, `per_adresse2`, `per_ville`, `per_cp`, `per_region`, `per_etablissement`, `per_mdp`)
VALUES (2, "2BC1944B-8AB6-23E0-56FF-99D80C918EA5", "Warren", "Jensen", "et", "at.velit.Pellentesque@metusIn.org",
        "08 38 80 87 90", "CP 976, 3194 Vel, Chemin", "CP 796, 3387 Nunc Route", "Ripalta Guerina", "HS9 3VQ", "LOM",
        "Ac Tellus Corp.", "Aenean"),
  (3, "A3470674-D17A-F012-993E-2333302D3BE5", "Kiara", "Padilla", "tincidunt",
   "Curabitur.dictum.Phasellus@venenatisvelfaucibus.net", "06 87 64 59 93", "Appartement 656-1403 Semper Ave",
   "996-9744 Non Av.", "Cametá", "38352", "Pará", "Venenatis A Ltd", "id"),
  (4, "7BDF5014-1F56-79B1-D476-A5585F7FC76D", "Fallon", "Soto", "sapien", "magna@risusQuisque.org", "02 15 30 51 82",
   "3760 Magna. Route", "CP 632, 1597 Egestas Av.", "Gasteiz", "V75 5PF", "Euskadi", "Nisi Nibh Lacinia Limited", "et"),
  (5, "91F90863-DE8B-16D5-EC6C-168A6A5DCFC2", "Debra", "Snider", "Quisque", "mattis@Nulladignissim.net",
   "05 18 73 81 06", "7058 Sem Chemin", "Appartement 920-2773 Ipsum Avenue", "Casper", "7760DA", "Wyoming",
   "Ante Dictum PC", "Mauris"),
  (6, "B4701C8D-7547-6926-0B4F-42FDFE7D297E", "Nichole", "Avila", "mi", "tortor.dictum@variusNam.edu", "01 92 28 96 55",
   "CP 273, 9745 Volutpat. Rd.", "Appartement 336-6643 Tincidunt Ave", "Charters Towers", "0948", "Queensland",
   "Vestibulum Consulting", "aliquet,");

INSERT INTO `PERSONNE` (`per_guid`, `per_prenom`, `per_nom`, `per_sexe`, `per_email`, `per_tel`, `per_adresse1`, `per_adresse2`, `per_ville`, `per_cp`, `per_region`, `per_etablissement`, `per_mdp`)
VALUES ("886E6F33-CE53-8E0A-3C5D-A871855C073F", "Elliott", "England", "Donec", "eu@interdumSed.edu", "09 96 98 72 15",
        "Appartement 857-7659 Praesent Avenue", "8767 Ultrices. Avenue", "Pocatello", "67654", "Idaho",
        "Mauris Non Dui Ltd", "sed"),
  ("925D2824-9DCD-D567-B9D0-4533297D0D8C", "Briar", "Smith", "massa.", "Sed.nunc@nuncsedlibero.net", "06 11 32 28 55", "912-3557 Gravida. Rd.", "CP 955, 4753 Amet Route", "Montebello", "80667", "QC", "Viverra LLC", "commodo"),
  ("E320753E-E6B2-9329-D0BB-5C653B696DCE", "Timothy", "Finch", "ac", "nisi.sem@Integertincidunt.com", "07 28 15 33 53", "816-2817 Sit Rd.", "Appartement 656-6784 Hendrerit Impasse", "Boston", "A1N 3V2", "MA", "Porttitor Industries", "tincidunt"),
  ("74477D6C-CC48-8F37-3923-12BD6C6E2BB5", "Xantha", "Delacruz", "molestie", "pharetra@scelerisquescelerisque.net", "08 47 69 82 05", "CP 346, 9045 At Avenue", "903-7709 In Impasse", "Płock", "388448", "MA", "Sapien Corporation", "Nam"),
  ("AE7E5EAE-BEEE-7ADA-FF6B-33F0C194A919", "Kellie", "Mcdaniel", "lorem", "amet.ante.Vivamus@egestasFuscealiquet.net", "09 24 29 68 22", "CP 509, 1247 Augue. Rd.", "CP 835, 1877 Vehicula. Impasse", "Caruaru", "1554", "Pernambuco", "Malesuada Id Erat Institute", "vel,"),
  ("8FD3F000-D092-121C-39DB-A8C4544C6739", "Shaeleigh", "Fulton", "eu", "rutrum.magna.Cras@pedesagittis.net", "04 69 49 61 03", "5584 Varius Chemin", "Appartement 689-971 Tristique Ave", "Richmond", "34404", "Quebec", "Arcu Imperdiet Ullamcorper Company", "fringilla."),
  ("F4A6587B-6BA7-51E0-5675-9956BB6C6D44", "Unity", "Turner", "urna", "felis.ullamcorper.viverra@rhoncusNullamvelit.net", "03 88 80 10 33", "Appartement 222-8756 Sed Rd.", "204-4054 Ante. Rue", "Carlisle", "31692", "Cumberland", "Pellentesque Habitant Foundation", "dictum"),
  ("990476C1-1E0D-DA20-25D5-474CE4BB6E56", "Howard", "Butler", "sem", "sociis.natoque@purus.edu", "01 74 21 62 54", "399-6945 Curabitur Avenue", "CP 351, 935 Lorem Ave", "Vienna", "48402", "Vienna", "Arcu Corporation", "magna."),
  ("99B773CE-F1A9-48E5-F9D0-D31736A1CD07", "Blaze", "Maxwell", "mollis.", "Fusce.aliquet@dictum.edu", "09 27 67 71 28", "7945 Sapien, Route", "Appartement 918-2182 Odio Route", "Whitehorse", "39626", "YK", "Semper Nam Tempor Institute", "lacinia"),
  ("F26E9D30-F273-615E-D23E-7119E4EB148E", "Ira", "Rosales", "quam", "est.tempor@risusDonecegestas.ca", "03 72 78 50 02", "7783 Eu Chemin", "440-4629 Placerat. Impasse", "Hollabrunn", "1317", "NÖ", "Vulputate Posuere Corporation", "facilisis,"),
  ("044883B3-AA9C-5C33-4E22-B71A633A7CCB", "Yetta", "Hutchinson", "eleifend", "Cras@posuerevulputate.org", "07 29 90 55 63", "CP 588, 4296 Enim, Rd.", "Appartement 975-415 Quis Chemin", "Berlin", "5065", "BE", "Amet Consectetuer Company", "massa."),
  ("EB48322C-3E78-0A2B-3269-7B8501FA25F5", "Hasad", "Gamble", "Donec", "lorem.Donec@varius.com", "05 40 69 07 87", "583-5257 Vel, Rd.", "2210 Pede. Chemin", "Pukekohe", "44156", "NI", "Sit Amet Risus Inc.", "ultrices,"),
  ("68D0D26D-6340-EEEB-E369-1248C5509649", "Audrey", "Sampson", "egestas.", "metus.Vivamus.euismod@risus.com", "01 34 21 17 06", "805-9764 Risus. Ave", "CP 756, 9008 Adipiscing Av.", "Calvi Risorta", "3034", "CAM", "Lacus Foundation", "sed"),
  ("B6592A8B-C7B0-1AE9-1BFC-826AAD359A72", "Nadine", "Montoya", "sem", "dui.augue@et.net", "09 33 78 74 03", "3844 Quis Ave", "CP 713, 3433 Ante Ave", "Radom", "81239", "MA", "Dolor Associates", "rhoncus."),
  ("8591F82A-A5C4-4FA9-4062-168CDF57C6A1", "Taylor", "Giles", "In", "Etiam.laoreet@amet.net", "06 94 55 54 24", "311 Eget Chemin", "2377 Purus, Chemin", "Mielec", "30311", "Podkarpackie", "Lectus Ante Foundation", "Nulla"),
  ("18EED2D5-CF04-E496-3F40-962FFBCC1201", "Christine", "Lambert", "Vivamus", "risus.odio@odiovel.com", "01 99 78 10 01", "5381 Metus. Chemin", "Appartement 209-8386 Cras Impasse", "Hamburg", "30-463", "HH", "Eu Ultrices Limited", "vestibulum."),
  ("4DE3A92A-E962-279A-5EE8-77862A467059", "Myles", "Cash", "nisi", "ante.blandit@nonmagnaNam.edu", "09 62 20 57 68", "817-8992 Ipsum Av.", "948-4548 Morbi Ave", "Watson Lake", "07643", "YK", "Tincidunt Corporation", "faucibus"),
  ("08AA86A0-39A3-3F08-0DC5-E653DAF8D268", "Jenna", "Rosa", "urna", "ut.ipsum@lorem.co.uk", "05 99 79 73 95", "Appartement 440-6764 Ut Route", "255-8321 At, Avenue", "Charters Towers", "90333", "Queensland", "Ipsum Non Arcu Associates", "eu"),
  ("82074ACF-99B1-D3C3-2B03-101A9E9F0199", "Madonna", "Caldwell", "commodo", "mus@diam.org", "06 39 40 94 56", "Appartement 816-6565 A Av.", "3198 Proin Route", "Thurso", "35187-735", "QC", "Eros Non Corporation", "vulputate,"),
  ("5CFD04BD-4095-FC24-8541-45C5ECB0F6E5", "Lysandra", "Cummings", "mauris.", "commodo.ipsum@lacusQuisque.net", "05 63 12 47 60", "569 Nisl. Rd.", "CP 624, 1558 Non Av.", "Weiz", "93488", "Styria", "Nec Enim LLC", "velit"),
  ("6C5242E4-3E2D-5EA4-CFB1-DE6B44D21CBD", "Bertha", "Schultz", "lacinia", "dictum.sapien@variusultrices.edu", "05 47 23 08 16", "Appartement 903-3813 Pede, Impasse", "Appartement 523-4428 Amet Rue", "King's Lynn", "7624", "NF", "Nisl Institute", "sagittis"),
  ("182FDFB5-DAF8-C3FB-C345-2C5D25DD461E", "Audrey", "Robinson", "ipsum", "Aenean.sed@semperauctor.com", "04 06 24 89 57", "4135 Viverra. Rue", "CP 563, 4544 Scelerisque Rd.", "Vienna", "7572", "Vienna", "Auctor Velit Aliquam Institute", "Donec"),
  ("ED4B80D5-7899-2279-DC2F-0620B3F796CC", "Hedda", "Patterson", "Proin", "vulputate.mauris.sagittis@Ut.com", "04 18 01 18 81", "Appartement 687-8751 Phasellus Rue", "Appartement 654-5193 Suspendisse Route", "Berlin", "55862", "BE", "Senectus Incorporated", "eget,"),
  ("C3F6CF2A-CE40-A679-C700-245815B8BC52", "Kaitlin", "Collins", "lacus.", "Nulla@CraspellentesqueSed.co.uk", "01 77 44 67 83", "748-4118 Maecenas Route", "CP 634, 7781 Magna Chemin", "Kaduna", "5528", "Kaduna", "Lobortis Consulting", "molestie"),
  ("935947F7-A42C-6AC1-3292-FCB9FB471310", "Dennis", "Fernandez", "molestie", "sed.turpis@maurisidsapien.net", "03 95 49 97 09", "CP 874, 2511 Id, Chemin", "709 Pede, Route", "Galway", "91405", "C", "Ac Eleifend Inc.", "accumsan"),
  ("EC56042E-37B0-2D1F-368D-675B3D515DF0", "Kiayada", "Harmon", "ante,", "velit@Quisqueporttitoreros.edu", "03 32 39 10 81", "612-705 Vivamus Chemin", "466-1637 Vel Impasse", "Flint", "20714", "FL", "Neque Et Institute", "magna"),
  ("ECE73504-354F-0725-94FF-004409F8A4CA", "Hiroko", "Juarez", "velit.", "enim.nisl.elementum@ipsumsodalespurus.org", "09 97 73 36 31", "CP 350, 9666 Accumsan Chemin", "Appartement 386-330 Ac Rd.", "Lagos", "509077", "LA", "Donec Tempor Limited", "mattis."),
  ("827A2F08-9D67-E42D-F63E-6C36CE7BCD7B", "Walker", "Francis", "non", "Sed.eget.lacus@vitae.com", "02 66 19 29 19", "Appartement 403-9966 Luctus, Route", "489-3689 Enim, Av.", "Valcourt", "8208HG", "Quebec", "Proin Incorporated", "dolor"),
  ("BF9E7E5D-E9E4-D05B-CE65-7D13DCF5E382", "Claudia", "Shannon", "tempus", "Vivamus@conubia.com", "08 86 92 01 42", "Appartement 988-2614 Velit. Impasse", "CP 664, 3695 Curabitur Rd.", "Veere", "660403", "Zeeland", "Lectus Nullam Suscipit LLP", "gravida."),
  ("3AB5BE52-7830-9F28-0E10-1CC62F78BDDB", "Sybill", "Baxter", "diam", "rutrum.non@scelerisque.co.uk", "06 36 07 22 22", "3038 Nulla Route", "CP 850, 7551 Pede Chemin", "Lauder", "852875", "BE", "Dui Augue PC", "Praesent"),
  ("942A6EB6-A0B2-D850-42A2-9EE4AD9C2B10", "Rhonda", "Underwood", "vel,", "ligula.elit.pretium@Quisque.org", "08 59 49 41 93", "Appartement 111-975 Felis Chemin", "CP 556, 1986 Magna. Av.", "Alsemberg", "3976", "VB", "Pellentesque Sed Dictum LLP", "elit,"),
  ("B6E2EE3C-D3CF-9D66-A263-D0F260C818E8", "Astra", "Cash", "venenatis", "Nam.nulla@elit.ca", "02 80 85 40 17", "Appartement 747-9534 Ornare, Rue", "306 Interdum Avenue", "Cheyenne", "1887EF", "WY", "Ipsum Non Arcu Industries", "sodales"),
  ("65C3D7BB-2B2B-5FD7-29EF-023944F0347B", "Lacey", "Dickerson", "accumsan", "habitant.morbi.tristique@odioNaminterdum.ca", "04 29 06 26 79", "280-4782 Enim Chemin", "542-6447 Posuere, Ave", "Sluis", "67814", "Zl", "Ipsum Non LLP", "sit"),
  ("D0B8E388-E3B5-CA40-8C62-37D1E055DD08", "Garth", "Slater", "vel", "eu.accumsan@Donec.edu", "07 04 11 15 03", "481-8270 Nec Impasse", "637 Sem Av.", "Cuenca", "65336", "CM", "In Sodales Elit PC", "lorem"),
  ("01F04F5A-9098-25BB-8958-05A5254C7AAB", "Porter", "Barrett", "ornare", "libero.et.tristique@metusVivamuseuismod.co.uk", "08 45 64 05 69", "CP 814, 8061 Facilisi. Chemin", "651-4671 Magna Chemin", "Lagos", "4884", "Lagos", "Dignissim Magna PC", "faucibus"),
  ("2B6B89F1-0F0C-C8CF-74E2-0176EC028A35", "Hedwig", "Jordan", "tempus,", "Nulla@Nuncullamcorper.edu", "05 96 26 51 87", "5619 Mi, Rd.", "616-638 A, Avenue", "Aparecida de Goiânia", "197659", "GO", "Massa Lobortis Limited", "lorem"),
  ("ECC683D6-D5D9-7CBE-2057-32B69E88F4D2", "Cole", "Clark", "Sed", "litora@acorciUt.co.uk", "05 70 55 38 32", "CP 309, 3123 Malesuada Route", "CP 136, 7626 Tincidunt Rue", "Niterói", "1535", "RJ", "Placerat Cras PC", "Donec"),
  ("1501BF41-B4F9-F411-14D9-458F2A58312F", "Evangeline", "Durham", "Aliquam", "mauris.sapien@ipsumacmi.net", "07 23 65 54 98", "282-5975 Curabitur Chemin", "7696 Aliquam, Rd.", "Tokoroa", "54-291", "North Island", "Sit LLC", "laoreet"),
  ("3AA032A7-8DD6-9430-2B14-1DA1BB47136C", "Yeo", "Mcfadden", "quis", "Aliquam.tincidunt@urna.co.uk", "04 94 29 93 35", "1359 Mi, Chemin", "Appartement 626-4204 Natoque Av.", "Oud-Turnhout", "6061", "Antwerpen", "Mi LLC", "Sed"),
  ("D3CCEF18-9D1D-A50C-6EBE-F41DF1D0FEC7", "Ingrid", "Thornton", "tincidunt", "sociis@posuerecubiliaCurae.co.uk", "05 19 97 90 44", "5687 Integer Rd.", "613-855 Dui. Chemin", "Drancy", "69105-817", "Île-de-France", "Lorem Foundation", "nibh."),
  ("A6020BE2-F1F0-4E53-CB50-F6B36088CF9D", "Zelda", "Schneider", "Sed", "et@ut.edu", "07 76 30 62 13", "CP 834, 3732 Nunc. Route", "CP 370, 7179 Ut, Impasse", "Musselburgh", "9136", "Midlothian", "Aliquam Nisl Industries", "arcu"),
  ("B69A9DCE-3E0D-6C36-0AE9-A0155DA61229", "Gregory", "Ward", "purus.", "malesuada@variusorciin.com", "09 23 51 53 13", "CP 331, 2208 Adipiscing. Rd.", "CP 954, 6618 At, Avenue", "Balfour", "74716", "Orkney", "Lacinia Associates", "sed"),
  ("88DD39A3-74B8-4BC2-CD51-8C2B0FA39105", "Darrel", "Lamb", "consectetuer", "eleifend.nunc@egestas.org", "04 64 53 62 62", "Appartement 987-5665 A Impasse", "6359 Iaculis Rd.", "Wortel", "399174", "AN", "Semper Pretium Neque Associates", "dui."),
  ("B34870D8-E9FA-AA7A-EBC8-50036C4ADC29", "Priscilla", "Valentine", "tempor", "ac.metus.vitae@pedeSuspendissedui.com", "07 72 96 81 75", "447-5953 Sed Avenue", "7360 Dolor Ave", "Kelso", "NK14 3QF", "RO", "Tellus Phasellus Inc.", "lacus."),
  ("484FD949-5B31-2C61-2374-A06FDD10F573", "Adara", "Kirkland", "eu", "ornare.In.faucibus@amet.com", "06 18 84 47 82", "CP 976, 6911 Orci. Route", "908-9875 Vivamus Avenue", "Thanjavur", "X0X 4Y0", "TN", "Non Bibendum Company", "nisi."),
  ("6C0322EF-86E1-E246-8719-A54233535369", "Lysandra", "Abbott", "sit", "semper.cursus@mi.com", "04 56 31 26 92", "618-617 Adipiscing Rue", "Appartement 719-8605 Non Rue", "Newport News", "K75 2FY", "Virginia", "Elit LLC", "netus"),
  ("ED5F5994-7497-D684-F821-EBD70E745192", "Jason", "Hudson", "ullamcorper", "adipiscing.Mauris@nuncid.edu", "04 39 35 17 20", "662 Nibh. Chemin", "4681 At Ave", "Veere", "20310", "Zeeland", "Ultricies Adipiscing Corporation", "Cras"),
  ("198EDE1E-EEAE-C1C9-174F-CC4BD97C32A7", "Hasad", "Avery", "ut,", "Nullam.velit.dui@Pellentesque.edu", "02 13 43 88 77", "2569 Tellus Ave", "245-1874 Odio Impasse", "Cork", "5415", "M", "Eget Inc.", "lacus."),
  ("75F05245-F234-AE88-29B2-7B3FA98FE1AD", "Lionel", "Ingram", "purus,", "ornare@ornare.org", "09 65 98 98 40", "Appartement 828-9068 Integer Rd.", "6086 Blandit. Av.", "Cork", "65-914", "M", "Eleifend Egestas Corp.", "Nam"),
  ("E69E2A12-7DA4-7D8D-86DB-3324D44CF662", "Minerva", "Johns", "libero.", "amet.luctus.vulputate@semelit.net", "03 78 82 81 71", "921-3478 Placerat. Rd.", "773-9762 Porttitor Av.", "Notre-Dame-du-Nord", "H4K 0R4", "Quebec", "Faucibus Inc.", "enim."),
  ("60FB7B35-6883-D158-AFD9-7E9392115A9A", "Macey", "Cotton", "metus", "iaculis.aliquet@nuncullamcorper.edu", "09 45 84 77 06", "3053 Mattis. Route", "123-5945 Bibendum. Rd.", "Watson Lake", "753907", "YT", "Nec Inc.", "dapibus"),
  ("D2B1C56C-4E4B-2157-87B9-D82F60BEBB19", "Amity", "Mathis", "in,", "feugiat@Aliquamerat.org", "08 88 55 79 32", "CP 800, 737 Feugiat Avenue", "3992 Dictum Av.", "Greymouth", "68426", "South Island", "Montes LLP", "metus."),
  ("A8CA5C28-8B7B-00D8-04A2-569A18905BAC", "Wayne", "Maynard", "eu,", "velit@Vivamus.co.uk", "09 76 03 79 14", "5517 Pede, Rd.", "CP 893, 7023 Amet Ave", "Vannes", "36537", "Bretagne", "Imperdiet Ullamcorper Duis PC", "risus."),
  ("EE5CA6F1-67A3-65C2-3FD5-3D6E85164722", "Venus", "Cote", "Morbi", "turpis.non@tincidunt.com", "06 05 00 52 43", "Appartement 478-3794 Nam Ave", "2720 Sit Route", "Kano", "935790", "KN", "Bibendum Donec Felis Company", "Suspendisse"),
  ("7F2F5887-D932-A31B-3FF1-4C9E1B71010B", "Cody", "Benton", "volutpat", "mollis.lectus@enimnislelementum.co.uk", "07 42 25 42 09", "396-3287 Suscipit, Chemin", "9816 Odio Impasse", "Milestone", "32-749", "Saskatchewan", "Orci Associates", "augue"),
  ("7155CD20-BBCB-C967-7754-490502390E53", "Nasim", "Miller", "penatibus", "ornare.facilisis@lectus.ca", "07 03 19 11 24", "Appartement 463-4985 Nunc. Ave", "292-4370 Rutrum Rue", "San Rafael Abajo", "55022", "SJ", "Neque Inc.", "pede,"),
  ("EBE8C6D0-10A4-23AC-9390-81A629BF8F32", "Benedict", "Underwood", "justo", "libero@sedfacilisis.com", "08 80 55 22 37", "1137 Ac Ave", "1708 Nostra, Rd.", "Alajuela", "3290", "A", "Ac Turpis Corporation", "augue"),
  ("C603F46E-21EA-54EF-54C3-FE698DD28903", "Raphael", "Mason", "amet", "eu@feugiatplaceratvelit.ca", "03 50 51 79 18", "2192 Ligula Rd.", "Appartement 148-2033 Aliquam Chemin", "Badajoz", "52-754", "EX", "Mi Lacinia Incorporated", "ut,"),
  ("4920B4BC-8F5B-471B-1A3B-C43ABC2C6B03", "April", "Lane", "at", "tincidunt@Quisquepurus.com", "08 81 73 05 65", "1158 Auctor Rue", "792-7401 Fermentum Av.", "Kester", "B8 3OV", "Vlaams-Brabant", "Erat Neque Corp.", "Nullam"),
  ("7D48C875-1EE9-CD98-713D-9373CC1CD9BC", "Nasim", "Navarro", "ornare", "lacus.Etiam@sedorci.net", "09 74 41 73 79", "CP 836, 126 Amet Rd.", "CP 436, 7559 Nunc Route", "Dallas", "02785-399", "Texas", "Fermentum Fermentum Company", "molestie"),
  ("EB291148-A849-839F-65E2-01E7483B51A8", "Harding", "Kaufman", "vitae", "erat@ipsumSuspendissenon.net", "06 57 24 85 00", "282-4373 Sem Impasse", "CP 229, 7207 Primis Impasse", "Aulnay-sous-Bois", "475655", "Île-de-France", "Donec Est Company", "Donec"),
  ("FC86C297-D4BC-5371-610F-8965203C16ED", "Kasimir", "Lamb", "vulputate", "taciti@aliquet.co.uk", "01 89 63 26 72", "Appartement 879-2513 Vulputate, Rd.", "Appartement 595-3159 Magna. Rue", "Toledo", "973966", "Castilla - La Mancha", "Tellus Eu Augue Institute", "In"),
  ("416FB119-58DD-B41B-4DBD-4A336BE58EFD", "Hall", "Cook", "eros.", "Etiam.bibendum@ornare.co.uk", "04 92 13 13 17", "Appartement 948-8253 Tellus. Rue", "CP 524, 2193 Mauris Route", "Cagliari", "424174", "SAR", "Sed Pharetra LLC", "tellus."),
  ("5C8BDC34-C5A6-9FCD-BB1F-337AD4F03919", "Zephania", "Horne", "elit.", "non.feugiat.nec@gravida.org", "03 49 28 81 05", "837-8082 Purus, Ave", "3339 Curabitur Rd.", "Evansville", "30051", "Indiana", "Ut Semper Pretium Institute", "semper"),
  ("86442428-050A-A90F-F80D-0773C0291608", "Martena", "Holman", "purus,", "Nullam@egestasSed.co.uk", "08 39 59 33 59", "CP 225, 3730 Arcu. Avenue", "5079 Facilisis Avenue", "Townsville", "76812", "Queensland", "Amet Consectetuer Incorporated", "ullamcorper"),
  ("E4B43B97-E495-104E-7D6E-A456305B7571", "Amethyst", "Gibbs", "est,", "in.molestie@eudolor.edu", "04 93 39 37 29", "2168 Ac, Route", "904-9895 Non Av.", "Lagos", "39782-919", "Lagos", "Elit Etiam Institute", "luctus"),
  ("37A6E17A-49B1-CC66-FF2E-5E2F713FCB8B", "Fletcher", "English", "Mauris", "ac.turpis@eteros.com", "02 07 31 51 99", "CP 163, 2511 Metus Route", "2100 Nunc Impasse", "Portree", "30127", "IN", "Nam Tempor Incorporated", "luctus"),
  ("DD5B5B30-2BC8-926C-9C79-F302994C9178", "Christian", "Boyle", "sapien", "amet.nulla@nislNulla.net", "01 84 23 98 02", "1050 Adipiscing Route", "CP 720, 4243 Donec Chemin", "Hompr�", "L1A 6A8", "Luxemburg", "Purus Maecenas Libero PC", "auctor,"),
  ("CC6C0C52-3BC7-CED3-51F8-D1ABD11EF9F0", "Darrel", "Boyer", "magna.", "velit.in@Nuncpulvinar.ca", "01 27 87 08 57", "CP 991, 6579 Sodales. Av.", "Appartement 680-3263 Gravida Av.", "Chicoutimi", "9056TW", "Quebec", "Pede Ac Urna LLP", "adipiscing"),
  ("1535EBEC-C0BD-C209-99E8-37C1D1816D76", "Martena", "Eaton", "iaculis", "metus.Vivamus.euismod@dictum.ca", "03 01 69 41 90", "Appartement 207-890 Ac Impasse", "368-4509 Dui. Rd.", "Sandy", "2121", "UT", "Suspendisse PC", "diam"),
  ("FE5F1B70-820C-CD47-97B0-6673F27C4A5F", "Rhea", "Camacho", "Aliquam", "tellus.eu@ametfaucibus.net", "06 71 42 19 16", "8657 Dis Impasse", "9665 Magna Ave", "Belém", "04304", "Pará", "Eget Massa Ltd", "sit"),
  ("637BA4AE-A5BB-3CA6-5DCD-164A22624566", "Nathan", "Mckinney", "neque", "enim.diam.vel@imperdiet.com", "01 76 26 42 22", "CP 676, 7015 Eu, Avenue", "Appartement 950-892 Ut Route", "Lelystad", "510875", "Fl", "Enim LLC", "aliquet,"),
  ("BA8223A0-3ABC-84EC-FC0A-2DA77E527537", "Hadley", "Osborn", "vel,", "nunc.sed.pede@tortoratrisus.ca", "03 38 70 91 42", "3708 Dapibus Chemin", "139-7050 Risus. Rue", "Sarreguemines", "03551", "LO", "Vestibulum Associates", "sagittis"),
  ("1A767CCE-64DC-EA64-65FD-A49F0C9CC427", "Willow", "Raymond", "ultricies", "facilisis.magna@bibendum.org", "05 66 33 86 69", "CP 109, 4589 Auctor Route", "Appartement 421-3598 Magna Route", "Middelburg", "21-455", "Zl", "Et Commodo Consulting", "bibendum."),
  ("7D4DD8F2-50AE-4379-C65C-4EBFF7B1795C", "Tasha", "Melendez", "dictum.", "Proin.vel@ipsumSuspendissesagittis.com", "01 88 43 08 69", "Appartement 934-9020 Congue Av.", "577-4709 Risus Av.", "Istanbul", "66703", "Istanbul", "Lacus Limited", "Proin"),
  ("C5CA5BAC-FDD1-AD5C-D2FB-81ECCBD83B0B", "Nolan", "Stein", "lacinia.", "Aliquam@tellusimperdiet.co.uk", "09 97 00 58 17", "Appartement 396-8035 Hendrerit Av.", "CP 575, 6203 Ultrices Route", "Oldenzaal", "31351", "Ov", "Sollicitudin Commodo Ipsum Company", "ultrices"),
  ("789887AD-8D55-D173-A622-15B35DA7D89F", "Pandora", "Michael", "tincidunt", "fames.ac.turpis@luctusetultrices.org", "08 21 82 35 31", "Appartement 124-2147 Ullamcorper. Route", "6392 Cursus, Rue", "Vizianagaram", "91449", "AP", "Enim Associates", "nunc"),
  ("DB54E454-D143-5EF7-36E4-46C693B87D93", "Acton", "Levy", "Etiam", "orci.lobortis.augue@Morbiquisurna.ca", "03 68 29 46 20", "1363 Sem Ave", "Appartement 178-3631 Ligula. Avenue", "Hamilton", "26946", "NI", "Egestas Duis Inc.", "eleifend"),
  ("29B0CB62-992C-25F9-24C2-E0CDFC632996", "Judah", "Lowe", "lectus,", "eu.dui.Cum@estacfacilisis.net", "03 02 41 18 42", "1123 Mauris Avenue", "CP 890, 8184 Proin Av.", "Sandy", "6885", "UT", "Id Company", "dui."),
  ("98D3794F-67F4-732A-3FBD-049881767189", "Nicholas", "Dalton", "nisi.", "erat.vitae@hendreritconsectetuer.org", "05 13 51 13 81", "CP 598, 6968 In Rue", "CP 657, 528 Urna. Chemin", "Reims", "41682-105", "Champagne-Ardenne", "Convallis Convallis Dolor Company", "ac"),
  ("DC413E03-3DE1-C9C4-DD1A-C31491D6A941", "Macy", "Franks", "felis", "vitae.semper@Donec.edu", "03 31 60 88 29", "672-7557 Faucibus. Rd.", "Appartement 552-3736 Ipsum Rue", "Gold Coast", "73781", "QLD", "A Purus Duis Limited", "vulputate"),
  ("76D98B54-B464-3DF5-E458-7F1A43CB277A", "Phelan", "Dawson", "Sed", "aliquet.molestie.tellus@iaculisodioNam.com", "09 84 92 51 77", "CP 200, 2523 Suspendisse Avenue", "498-1429 Suspendisse Rd.", "Salem", "7716", "Tamil Nadu", "Nec LLP", "urna"),
  ("2DE2B07B-CBA8-91FD-FBDB-6FC82FA23DB7", "Silas", "Stephenson", "aliquam", "vitae.aliquam.eros@auctorvelit.org", "03 67 35 07 85", "391-1080 Adipiscing Chemin", "756-2311 Fringilla, Avenue", "Belfast", "3734EQ", "Ulster", "Nulla Foundation", "metus"),
  ("EF51C1CB-957C-4B56-F20E-39B97D8B57A9", "Zephr", "Hudson", "Integer", "dictum@estNunclaoreet.net", "03 33 32 75 32", "111-5476 Lectus Route", "CP 354, 8776 Ac Rue", "Shaki", "27402", "OY", "Accumsan LLP", "erat"),
  ("F2525967-73D8-3D92-FCB6-F37250235CCE", "Roanna", "Holden", "ac,", "nec@ullamcorper.edu", "06 89 06 75 26", "9407 Mauris. Impasse", "Appartement 405-8676 Tempus Chemin", "Sant'Angelo a Cupolo", "587658", "Campania", "Eu Eros Corp.", "varius"),
  ("DA16CC59-801B-0E67-C04B-F574DAD73BC6", "Yoshio", "Craig", "vehicula.", "vulputate@augueSed.edu", "06 32 11 59 94", "Appartement 600-391 Amet Rd.", "3318 Nulla Rd.", "Berlin", "3195", "Berlin", "Eleifend Nec Malesuada LLP", "arcu."),
  ("6D80F9F4-E5E9-488D-64F3-9E543D099319", "Lewis", "Jones", "dapibus", "neque.sed@Vestibulumante.org", "02 92 01 28 93", "4861 Nonummy Av.", "672-2594 Etiam Rd.", "Whangarei", "434163", "North Island", "Maecenas Malesuada Fringilla Consulting", "a"),
  ("CA440A34-12FE-A6D5-61B2-B1246670D779", "Germane", "Bird", "auctor", "dolor@dolorFuscemi.co.uk", "06 14 58 23 45", "939-683 Dapibus Avenue", "3396 Felis Route", "Dunoon", "8953", "Argyllshire", "Nibh Corp.", "nisl."),
  ("DB62249A-5141-8ACD-0ED7-9737FD4AC326", "Caryn", "Yang", "nonummy", "semper.auctor.Mauris@luctusCurabitur.edu", "03 32 33 84 37", "Appartement 855-927 Et Av.", "847-7858 Velit Rd.", "Abergele", "11801", "Denbighshire", "Auctor Non LLC", "euismod"),
  ("8170E956-DF10-AF83-E4C4-EF3154416A7D", "Mohammad", "Combs", "quis,", "vel@facilisisnon.net", "08 99 48 53 44", "Appartement 565-6319 Non, Av.", "CP 778, 1765 Eleifend Rd.", "San Rafael", "20505", "A", "Ut Ipsum Ac Associates", "feugiat"),
  ("BDE19942-46A0-C229-47D3-8C2C42DD5678", "Sydney", "Acevedo", "scelerisque,", "non@nec.com", "09 52 37 89 51", "Appartement 777-100 Sed Chemin", "Appartement 730-1709 Vehicula Route", "Cobourg", "6163", "ON", "Phasellus At Augue Institute", "facilisis."),
  ("1B210A45-4ED1-1FC6-0F70-787E5877CFE2", "Courtney", "Holcomb", "Nunc", "eros.non@tristiquealiquet.org",
   "02 73 70 53 99", "Appartement 997-6723 Tempus Avenue", "947-174 Purus. Impasse", "Chicoutimi", "OF0H 1YH", "Quebec",
   "Semper Dui LLP", "mattis."),
  ("B99AF282-6C57-171C-C1D7-3F94D11BC202", "Pascale", "Stone", "vel", "orci.luctus.et@euismod.net", "03 43 72 17 38",
   "2744 Purus, Ave", "Appartement 422-3964 Litora Av.", "Bayonne", "26220", "Aquitaine",
   "Adipiscing Fringilla Porttitor Foundation", "vestibulum"),
  ("872CDFFD-7B76-312E-D944-50BE6A20B495", "Whitney", "Petersen", "Integer", "risus.at.fringilla@auguemalesuada.edu",
   "04 59 47 98 18", "3419 Pede, Impasse", "Appartement 121-5938 At Impasse", "Carluke", "62-735", "LK",
   "Posuere Vulputate Lacus Corporation", "ut"),
  ("077C14F7-0F92-645E-32B3-D39370B8E355", "Cyrus", "Hatfield", "vel", "magna.Suspendisse@tristique.co.uk",
   "05 02 02 39 77", "CP 809, 1184 Massa. Rue", "9494 At, Chemin", "Oklahoma City", "62207", "Oklahoma",
   "Rutrum Non LLP", "ultrices"),
  ("941044A0-4FD7-0484-86C8-50C9EDDB3D03", "Bradley", "Pratt", "semper", "auctor.nunc@gravidasagittisDuis.org",
   "01 18 38 86 34", "3494 Gravida Ave", "522-7710 Mollis Avenue", "Dublin", "67915", "Leinster",
   "Proin Vel Arcu Foundation", "molestie"),
  ("7486952C-9F16-EDBC-B29B-7DCF202FB82C", "Bruno", "Rutledge", "dignissim", "ac.orci.Ut@fringillaornareplacerat.edu",
   "07 31 65 95 26", "301-1285 Orci Impasse", "CP 842, 216 Dictum Ave", "Mysore", "20502", "Karnataka", "Iaculis Inc.",
   "Sed"),
  ("F0752FE6-31A2-8A87-F39D-979ACDCA8324", "Ella", "James", "Nunc", "neque.sed.dictum@rutrumeu.co.uk", "05 01 06 52 08",
   "Appartement 867-7315 Tristique Ave", "Appartement 690-8693 Inceptos Impasse", "G�rompont", "N2L 1X2",
   "Waals-Brabant", "Erat Vitae Risus Associates", "non"),
  ("A492BC75-84D8-7885-D2C1-80E5DCF25ECB", "Barry", "Mooney", "mauris", "pellentesque.a.facilisis@ornareegestas.co.uk",
   "07 03 82 45 08", "Appartement 536-3394 Augue Route", "152-6260 Consectetuer Chemin", "Sherbrooke", "82426",
   "Quebec", "Duis LLC", "eget,"),
  ("2CD7E25D-3A7B-A8FA-4EFF-5469DBA00643", "Rhea", "Bolton", "et", "pharetra@Crasdolor.com", "01 90 94 32 29",
   "979-3371 Malesuada Route", "1743 Consequat Chemin", "Cork", "3040DP", "Munster",
   "Turpis In Condimentum Incorporated", "mauris,");

#-- STAFF avec comme mdp staffpw
INSERT INTO `STAFF` (`sta_id`, `sta_prenom`, `sta_nom`, `sta_email`, `sta_tel`, `sta_mdp`, `sta_ts_id`)
VALUES (1, "Brynne", "English", "id.ante@vulputate.co.uk", "07 77 41 42 92", "staffpw", 1),
  (2, "Aubrey", "Hobbs", "in.felis.Nulla@augue.edu", "05 52 72 71 61", "staffpw", 1),
  (3, "Amity", "Wells", "support@rondedesfacs.fr", "08 36 84 43 87", "staffpw", 3),
  (4, "Dr. Scoubidou", "", "malesuada.augue.ut@sitametrisus.ca", "05 99 86 53 44", "staffpw", 2),
  (5, "OSN", "", "sagittis.placerat@Nullamvelit.co.uk", "02 11 63 48 66", "staffpw", 2);


INSERT INTO COURSE (cou_id, cou_libelle, cou_date_ins_debut, cou_date_ins_fin, cou_date_deroulement, cou_participants_max,
                    cou_nb_relais, cou_distance, cou_sc_id)
VALUES
  (1, 'Rondes des Facs 2013', '01/01/2013', '01/03/2013', '30/03/2013', 300, 2, 2, 6),
  (2, 'Rondes des Facs 2014', '01/01/2014', '01/03/2014', '30/03/2014', 300, 2, 2, 7),
  (3, 'Rondes des Facs 2015 Q1', '01/01/2015', '01/03/2015', '30/03/2015', 300, 2, 2, 5),
  (4, 'Rondes des Facs 2015 Q2', '01/01/2015', '01/04/2015', SYSDATE(), 300, 2, 2, 4),
  (5, 'Rondes des Facs 2015 Man', '01/01/2015', '01/04/2015', '01/09/2015', 300, 2, 2, 3),
  (6, 'Rondes des Facs 2015 Women', '01/01/2015', '01/08/2015', '01/11/2015', 300, 2, 2, 2),
  (7, 'Rondes des Facs 2016', '01/01/2016', '01/04/2016', '30/03/2016', 300, 2, 2, 1);

INSERT INTO PARTICIPANT (par_per_id, par_cou_id, par_num_dossard, par_certificat, par_inscrit)
VALUES (1, 1, 1, NULL, TRUE);