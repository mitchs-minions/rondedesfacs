#include "SmardCardException.h"

SmardCardException::SmardCardException(LONG ErrCode,string MethodeName)
{
	this->ErrorCode = ErrCode;

	std::string number;
	std::stringstream strstream;
	strstream << ErrorCode;
	strstream >> number;

	Message = "Erreur Methode : "+MethodeName+" code d'erreur : "+number+" Message : "+GetErrorMessage(ErrorCode);
}

string SmardCardException::GetErrorMessage(LONG err)
{
	string s;
	switch(err)
	{
		case ERROR_BROKEN_PIPE:
			s = "The client attempted a smart card operation in a remote session, such as a client session running on a terminal server, and the operating system in use does not support smart card redirection.";
			break;
		case SCARD_E_BAD_SEEK:
			s = "An error occurred in setting the smart card file object pointer.";
			break;
		case SCARD_E_CANCELLED:
			s ="The action was canceled by an SCardCancel request";
			break;
		case SCARD_E_CANT_DISPOSE:
			s ="The system could not dispose of the media in the requested manner";
			break;
		case SCARD_E_CARD_UNSUPPORTED:
			s ="The smart card does not meet minimal requirements for support";
			break;
		case SCARD_E_CERTIFICATE_UNAVAILABLE:
			s ="The requested certificate could not be obtained";
			break;
		case SCARD_E_COMM_DATA_LOST:
			s="A communications error with the smart card has been detected";
			break;
		case SCARD_E_DIR_NOT_FOUND:
			s ="The specified directory does not exist in the smart card";
			break;
		case SCARD_E_DUPLICATE_READER:
			s ="The reader driver did not produce a unique reader name";
			break;
		case SCARD_E_FILE_NOT_FOUND:
			s ="The specified file does not exist in the smart card";
			break;
		case SCARD_E_ICC_CREATEORDER:
			s ="The requested order of object creation is not supported";
			break;
		case SCARD_E_ICC_INSTALLATION:
			s="No primary provider can be found for the smart card";
			break;
		case SCARD_E_INSUFFICIENT_BUFFER:
			s ="The data buffer for returned data is too small for the returned data";
			break;
		case SCARD_E_INVALID_ATR:
			s="An ATR string obtained from the registry is not a valid ATR string";
			break;
		case SCARD_E_INVALID_CHV:
			s ="The supplied PIN is incorrect";
			break;
		case SCARD_E_INVALID_HANDLE:
			s ="The supplied handle was not valid";
			break;
		case SCARD_E_INVALID_PARAMETER:
			s="One or more of the supplied parameters could not be properly interpreted";
			break;
		case SCARD_E_INVALID_TARGET:
			s="Registry startup information is missing or not valid";
			break;
		case SCARD_E_INVALID_VALUE:
			s="One or more of the supplied parameter values could not be properly interpreted";
			break;
		case SCARD_E_NO_ACCESS:
			s="Access is denied to the file";
			break;
		case SCARD_E_NO_DIR:
			s ="The supplied path does not represent a smart card directory";
			break;
		case SCARD_E_NO_FILE:
			s ="The supplied path does not represent a smart card file";
			break;
		case SCARD_E_NO_KEY_CONTAINER:
			s ="The requested key container does not exist on the smart card";
			break;
		case SCARD_E_NO_MEMORY:
			s="Not enough memory available to complete this command";
			break;
		case SCARD_E_NO_PIN_CACHE:
			s ="The smart card PIN cannot be cached";
			break;
		case SCARD_E_NO_READERS_AVAILABLE:
			s="No smart card reader is available";
			break;
		case SCARD_E_NO_SERVICE:
			s ="The smart card resource manager is not running";
			break;
		case SCARD_E_NO_SMARTCARD:
			s ="The operation requires a smart card, but no smart card is currently in the device";
			break;
		case SCARD_E_NO_SUCH_CERTIFICATE:
			s ="The requested certificate does not exist";
			break;
		case SCARD_E_NOT_READY:
			s ="The reader or card is not ready to accept commands";
			break;
		case SCARD_E_NOT_TRANSACTED:
			s="An attempt was made to end a nonexistent transaction";
			break;
		case SCARD_E_PCI_TOO_SMALL:
			s ="The PCI receive buffer was too small";
			break;
		case SCARD_E_PIN_CACHE_EXPIRED:
			s ="The smart card PIN cache has expired";
			break;
		case SCARD_E_PROTO_MISMATCH:
			s ="The requested protocols are incompatible with the protocol currently in use with the card";
			break;
		case SCARD_E_READ_ONLY_CARD:
			s ="The smart card is read-only and cannot be written to";
			break;
		case SCARD_E_READER_UNAVAILABLE:
			s ="The specified reader is not currently available for use";
			break;
		case SCARD_E_READER_UNSUPPORTED:
			s ="The reader driver does not meet minimal requirements for support";
			break;
		case SCARD_E_SERVER_TOO_BUSY:
			s ="The smart card resource manager is too busy to complete this operation";
			break;
		case SCARD_E_SERVICE_STOPPED:
			s ="The smart card resource manager has shut down";
			break;
		case SCARD_E_SHARING_VIOLATION:
			s ="The smart card cannot be accessed because of other outstanding connections";
			break;
		case SCARD_E_SYSTEM_CANCELLED:
			s ="The action was canceled by the system, presumably to log off or shut down";
			break;
		case SCARD_E_TIMEOUT:
			s ="The user-specified time-out value has expired";
			break;
		case SCARD_E_UNEXPECTED:
			s="An unexpected card error has occurred";
			break;
		case SCARD_E_UNKNOWN_CARD:
			s ="The specified smart card name is not recognized";
			break;
		case SCARD_E_UNKNOWN_READER:
			s ="The specified reader name is not recognized";
			break;
		case SCARD_E_UNKNOWN_RES_MNG:
			s ="An unrecognized error code was returned";
			break;
		case SCARD_E_UNSUPPORTED_FEATURE:
			s ="This smart card does not support the requested feature";
			break;
		case SCARD_E_WRITE_TOO_MANY:
			s ="An attempt was made to write more data than would fit in the target object";
			break;
		case SCARD_F_COMM_ERROR:s ="An internal communications error has been detected";
			break;
		case SCARD_F_INTERNAL_ERROR:
			s ="An internal consistency check failed";
			break;
		case SCARD_F_UNKNOWN_ERROR:
			s ="An internal error has been detected, but the source is unknown";
			break;
		case SCARD_F_WAITED_TOO_LONG:
			s ="An internal consistency timer has expired";
			break;
		case SCARD_P_SHUTDOWN:
			s ="The operation has been aborted to allow the server application to exit";
			break;
		case SCARD_S_SUCCESS:
			s ="No error was encountered";
			break;
		case SCARD_W_CANCELLED_BY_USER:
			s ="The action was canceled by the user";
			break;
		case SCARD_W_CACHE_ITEM_NOT_FOUND:
			s ="The requested item could not be found in the cache";
			break;
		case SCARD_W_CACHE_ITEM_STALE:
			s ="The requested cache item is too old and was deleted from the cache";
			break;
		case SCARD_W_CACHE_ITEM_TOO_BIG:
			s ="The new cache item exceeds the maximum per-item size defined for the cache";
			break;
		case SCARD_W_CARD_NOT_AUTHENTICATED:
			s ="No PIN was presented to the smart card";
			break;
		case SCARD_W_CHV_BLOCKED:
			s ="The card cannot be accessed because the maximum number of PIN entry attempts has been reached";
			break;
		case SCARD_W_EOF:
			s ="The end of the smart card file has been reached";
			break;
		case SCARD_W_REMOVED_CARD:
			s ="The smart card has been removed, so further communication is not possible";
			break;
		case SCARD_W_RESET_CARD:
			s ="The smart card was reset";
			break;
		case SCARD_W_SECURITY_VIOLATION:
			s ="Access was denied because of a security violation";
			break;
		case SCARD_W_UNPOWERED_CARD:
			s ="Power has been removed from the smart card, so that further communication is not possible";
			break;
		case SCARD_W_UNRESPONSIVE_CARD:
			s ="The smart card is not responding to a reset";
			break;
		case SCARD_W_UNSUPPORTED_CARD:
			s ="The reader cannot communicate with the card, due to ATR string configuration conflicts";
			break;
		case SCARD_W_WRONG_CHV:
			s ="The card cannot be accessed because the wrong PIN was presented";
			break;
		default:
			s="Unable to find the Error";
			break;
	}
	return s;
}

string SmardCardException::getMessage()
{
	return Message;
}