#include <string>
#include <sstream>
#include "WinScard.h"

using namespace std;

class SmardCardException : public exception
{
private:
	string Message;
	LONG ErrorCode;

	string GetErrorMessage(LONG ErrorCode);

public:
	SmardCardException(LONG ErrorCode,string MethodeName);
	string getMessage();
};