<?php

/**
* Récupère les résultats via BDD
* Après via Web Service
*/
class Resultat
{
	
	static public function getLesResultats()
	{
		global $connexion;
		$lesCourses = $connexion->query("SELECT * FROM `PASSAGE`");

		if($lesCourses->rowCount() > 0) {
			$json = array();
			while($row = $lesCourses->fetch()){
				$json[] = array();
			}
			$response = array();
			$response['success'] = true;
			$response['aaData'] = $json;
			$dataJson = json_encode($response);
		}

		$fichier = "web/ajax/resultat.txt";


		// Si fichier existe on le supprime
		if (file_exists($fichier)) {
			unlink($fichier);
		}

		$course = fopen($fichier, "w+");

		fgets($course); // On lit le fichier
		fwrite($course, $dataJson); // On écrit dans le fichier les résultats de la requête		
		fclose($course);
	}
}

?>