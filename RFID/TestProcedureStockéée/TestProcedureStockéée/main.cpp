#include "RequeteBDD.h"

int main(int argc,char*argv[])
{
	
	
	try
	{	
		if(argc != 4 && argc != 5)
		{
			Console::WriteLine("wrong amount of argument");
			return -1;
		}

		int proc = atoi(argv[1]);

		if(proc == 1)
		{
			RequeteBDD*req = new RequeteBDD();
			req->ajouterPassage(argv[2],atoi(argv[3]),atoi(argv[4]));
		}
		else if(proc == 2)
		{
			RequeteBDD*req = new RequeteBDD();
			req->appareillerCarte(argv[2],atoi(argv[3]));
		}
		else
		{
			Console::WriteLine("wrong proc number");
			return -1;
		}

		return 0;

		Console::WriteLine("done");
	}
	catch(OdbcException^myEx)
	{
		 for (int i=0;i<myEx->Errors->Count;i++)
		 {
			Console::WriteLine("Source:{0};Message={1}",myEx->Errors->default[i]->Source,myEx->Errors->default[i]->Message);
		}
	}
	catch(System::Exception^myEx)
	{
	   Console::WriteLine("Source:{0};Message={1}",myEx->Source,myEx->Message);
	}
	
	return 0;
}