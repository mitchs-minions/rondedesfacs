<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 10/04/2015
 * Time: 14:28
 */

// initialise la connexion
include_once('config_init.php');

if (isset($_GET["action"])) {

    if ($_GET["action"] == 'getListeCourses') {

        $stmt = $connexion->prepare("SELECT cou_id courseId, cou_libelle courseLibelle FROM course ORDER BY cou_libelle");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $stmt->fetch()) {
            echo "<option value='" . $row["courseId"] . "'>" . $row["courseLibelle"] . "</option>";
        }
    }

    if ($_GET["action"] == 'getListeParticipants') {
        if (isset($_GET["courseId"])) {
            $stmt = $connexion->prepare("SELECT per_id parId, per_prenom parPrenom, per_nom parNom, per_email parEmail
                                        FROM personne, participant
                                        WHERE personne.per_id = participant.par_per_id
                                        AND participant.par_cou_id = :courseId
                                        ORDER BY cou_libelle");
            $stmt->bindParam(':courseId', $_GET["courseId"]);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            while ($row = $stmt->fetch()) {
                echo "<option value='" . $row["parId"] . "'>" . $row["parPrenom"] . " " . $row["parNom"] . " (" . $row["parEmail"] . ")" . "</option>";
            }
        }

    }
}

?>
