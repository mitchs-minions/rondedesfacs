<?php
require("config_init.php");

if (isset($_POST) && isset($_POST["inscription"])) {
	// récupérer les éléments du formulaire
	// Générer mot de passe + mon grain de sel
	$prefixSalt = "YTRjNjE3OThmNzNkM2Y2MTliZTFiYjNi";
	$suffixSalt = "OTcwMzUyZTgzNGFiYWNkZGI5NTYwZWU5";

	$email = stripslashes($_POST['email']);
	$pass = stripslashes($_POST['password']);
	$password = md5($prefixSalt.$pass.$suffixSalt);

	if (array_key_exists('civilite',$_POST)) {
		$civilite = ucfirst(stripslashes($_POST['civilite']));
	}

	$nom = stripslashes($_POST['nom']);
	$prenom = stripslashes($_POST['prenom']);
	$tel = stripslashes($_POST['tel']);
	$adresse1 = stripslashes($_POST['adresse1']);
	$adresse2 = stripslashes($_POST['adresse2']);
	$ville = stripslashes($_POST['ville']);
	$cp = stripslashes($_POST['cp']);
	$region = stripslashes($_POST['region']);

	try {
		// Si l'email est déjà existant
		$sql = $connexion->query("SELECT * FROM `PERSONNE` AS personne WHERE `personne`.`per_email`= '".$email."'");
		if ($sql->fetchColumn() >= 1) {
			header("Location: ../index.php?erreurI&nom=".$nom."&prenom=".$prenom."&civilite=".$civilite);
		}
		else {
			// Tenter d'inscrire l'utilisateur dans la base
			$sql = $connexion->prepare("INSERT INTO `PERSONNE` (per_email, per_mdp, per_civilite, per_nom, per_prenom, per_tel, per_adresse1, per_adresse2, per_ville, per_cp, per_region) "
			. "VALUES (:email, :password, :civilite, :nom, :prenom, :tel, :adresse1, :adresse2, :ville, :cp, :region)");
			$sql->bindValue("email", $email);
			$sql->bindValue("password", $password);
			$sql->bindValue("civilite", $civilite);
			$sql->bindValue("nom", $nom);
			$sql->bindValue("prenom", $prenom);
			$sql->bindValue("tel", $tel);
			//$sql->bindValue("etablissement", $etablissement);
			$sql->bindValue("adresse1", $adresse1);
			$sql->bindValue("adresse2", $adresse2);
			$sql->bindValue("ville", $ville);
			$sql->bindValue("cp", $cp);
			$sql->bindValue("region", $region);

			// insertion base de donnée dans membre

			if (!$sql->execute()) {
				$err = $sql->errorInfo();
				print_r($err);
			} else {
				$requete = "SELECT * FROM `PERSONNE` AS personne WHERE `personne`.`per_email`= '".$email."'";
				$sql = $connexion->query($requete);
				if ($sql->fetchColumn() < 1) {
					header("Location: ../index.php?erreurI");
				}
				else {
					// ici démarrer une session
					session_start();
					try {
						$sql = $connexion->query($requete);
						// on récupère la ligne qui nous intéresse avec $sql->fetch(),
						while ($data = $sql->fetch(PDO::FETCH_ASSOC)) {
							$_SESSION["id"] = $data["per_id"];
							$_SESSION["email"] = $data["per_email"];
							$_SESSION["password"] = $password;
							$_SESSION["nom"] = $data["per_nom"];
							$_SESSION["prenom"] = $data["per_prenom"];
							$_SESSION["sexe"] = $data["per_sexe"];
							$_SESSION["tel"] = $data["per_tel"];
							$_SESSION["adresse1"] = $data["per_adresse1"];
							$_SESSION["adresse2"] = $data["per_adresse2"];
							$_SESSION["ville"] = $data["per_ville"];
							$_SESSION["cp"] = $data["per_cp"];
							$_SESSION["region"] = $data["per_region"];
						}
					}
					catch(Exception $e) {
						trigger_error($e->getMessage(), E_USER_ERROR);
					}
				}
				header("Location: ../index.php?isco");
			}
			$connexion = null;
		}
	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		$connexion = null;
		die();
	}
} else {
	header("Location: ../index.php?erreurI");
}
?>