<html>
<head>
	<meta charset="UTF-8">
	<title>Connexion | Ronde Des Facs</title>
</head>
<body>
	<form action="../controllers/connexion.php" method="POST" name="login" id="login">
		<p>
			<li>
				<input type="email" name="email" id="email" pattern="[a-zA-Z0-9À-ŷ.!#$%&’*+/=?^_`{|}~-]+\@[a-zA-Z0-9]{4,}\.[a-zA-Z0-9]{2,4}" placeholder="Email" autofocus value="lastennet.l@gmail.com" required/>
				<label for="email">Identifiant</label>
			</li>
			<li>
				<input type="password" name="password" id="mdp1" pattern="[a-zA-Z0-9\s]{6,15}" maxlength="15" title="Le mot de passe doit être de 6 à 15 caractères alphanumériques !" value="123456" placeholder="6 à 15 caractères" required/>
				<label for="mdp1">Mot de passe</label>
			</li>
		</p>
		<p>
			<input type="submit" id="connect" name="connect" value="Valider"/>
		</p>
	</form>
</body>
</html>