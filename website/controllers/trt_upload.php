<?php

require("config_init.php");

if (isset($_POST) && isset($_POST["upload"])) {

	$nomOrigine = $_FILES['ajouter']['name'];
	$elementsChemin = pathinfo($nomOrigine);
	$extensionFichier = $elementsChemin['extension'];
	$extensionsAutorisees = array("jpeg", "jpg", "gif", "png", "pdf");
	if (!(in_array($extensionFichier, $extensionsAutorisees))) {
	echo "Le fichier n'a pas l'extension attendue";
	} else {    
		// Copie dans le repertoire du script avec un nom
		// incluant l'heure a la seconde pres 
		$repertoireDestination = "../web/certificat/";
		$nomDestination = "Certificat".date("YmdHis").".".$extensionFichier;

		if (move_uploaded_file($_FILES["ajouter"]["tmp_name"], 
			$repertoireDestination.$nomDestination)) {
			/*echo "Le fichier temporaire ".$_FILES["ajouter"]["tmp_name"].
				" a été déplacé vers ".$repertoireDestination.$nomDestination;*/
			header("Location: index.php?upload=1");
		} else {
			/*echo "Le fichier n'a pas été uploadé (trop gros ?) ou ".
				"Le déplacement du fichier temporaire a échoué".
				" vérifiez l'existence du répertoire ".$repertoireDestination;*/
				header("Location: index.php?upload=0");
		}
	}

}

?>