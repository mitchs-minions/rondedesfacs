#include "WinScard.h"
#include <iostream>
#include <string>
#include "SmardCardException.h"

using namespace std;

class SmardCard
{
private:
	SCARDHANDLE connectionIdentifier;
	LPTSTR readerList;
	SCARDCONTEXT contextHandle;

public:
	SmardCard();
	~SmardCard();
	int*getCardId(LPTSTR reader);
	LPTSTR getReaderList();
};


/*
	SCARDCONTEXT hSC;
	LONG lReturn;
	LPTSTR pmszReaders = NULL;
	DWORD cch = SCARD_AUTOALLOCATE;
	DWORD dwAP;
	DWORD dwRecvLength;
	SCARDHANDLE hCardHandle;
	SCARD_IO_REQUEST pioSendPci;

	WCHAR szReader[200];
	DWORD ReaderLen;
	BYTE bAttr[32];
	DWORD cByte ;
	DWORD dwState , dwProtocol ;

	 BYTE pbRecvBuffer[258];
	 BYTE cmd1[256];
	 BYTE LoadKey[] = "\x80\x06\x10\x07\x04\x0A\x10\xFF";
	 BuildAPDU(cmd1,LoadKey,sizeof(LoadKey));

	lReturn = SCardEstablishContext(SCARD_SCOPE_USER,NULL,NULL,&hSC);
	lReturn = SCardListReaders(hSC,NULL,(LPTSTR)&pmszReaders,&cch);
	lReturn = SCardConnect(hSC,pmszReaders,SCARD_SHARE_SHARED,SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,&hCardHandle,&dwAP );

	switch(dwAP)
	 {
	  case SCARD_PROTOCOL_T0:
	   pioSendPci = *SCARD_PCI_T0;
	   break;

	  case SCARD_PROTOCOL_T1:
	   pioSendPci = *SCARD_PCI_T1;
	   break;
	 }

	lReturn = SCardBeginTransaction(hCardHandle);
	lReturn = SCardStatus(hCardHandle,szReader,&ReaderLen,&dwState,&dwProtocol,(LPBYTE)&bAttr,&cByte);
	 dwRecvLength = sizeof(pbRecvBuffer);
	 lReturn = SCardTransmit(hCardHandle, &pioSendPci, cmd1, sizeof(cmd1),NULL, pbRecvBuffer, &dwRecvLength);



*/