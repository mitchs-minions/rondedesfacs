<?php
require("config_init.php");

if (isset($_POST) && isset($_POST["modifier"])) {

	extract($_POST);

	// Modifie le profil de la personne
	$sql = $connexion->prepare("UPDATE `PERSONNE` SET `per_email` = :email, `per_nom` = :nom, `per_prenom` = :prenom, `per_tel` = :tel, `per_adresse1` = :adresse1, `per_adresse2` = :adresse2, `per_ville` = :ville, `per_cp` = :cp, `per_region` = :region WHERE `per_id` = :id");
	$sql->bindValue("email", $email);
	$sql->bindValue("nom", $nom);
	$sql->bindValue("prenom", $prenom);
	$sql->bindValue("tel", $tel);
	$sql->bindValue("adresse1", $adresse1);
	$sql->bindValue("adresse2", $adresse2);
	$sql->bindValue("ville", $ville);
	$sql->bindValue("cp", $cp);
	$sql->bindValue("region", $region);
	$sql->bindValue("id", $_SESSION["id"]);
	$sql->execute();

	header("Location: fonction.php?modif");

}

elseif (isset($_POST) && isset($_POST["modifierPass"])) {

	extract($_POST);

	$prefixSalt = "YTRjNjE3OThmNzNkM2Y2MTliZTFiYjNi";
	$suffixSalt = "OTcwMzUyZTgzNGFiYWNkZGI5NTYwZWU5";

	$pass = stripslashes($password);
	$password = md5($prefixSalt.$pass.$suffixSalt);

	// Modifie seulement le mot de passe de la personne
	$sql = $connexion->prepare("UPDATE `PERSONNE` SET `per_mdp` = :password WHERE `per_id` = :id");
	$sql->bindValue("password", $password);
	$sql->bindValue("id", $_SESSION["id"]);
	$sql->execute();

	header("Location: fonction.php?modif");

}

?>