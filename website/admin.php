<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Espace - Administration</title>

	<meta name="author" content="">
	<meta name="description" content="Ronde des facs">
	<meta name="geo.placename" content="Nice, PACA, Cote-d'Azur,France">
	<meta name="keywords" content="Ronde des facs,Etudiant,Université Sophia Antipolis,Course,4km">
	<meta name="copyright" content="">
	<meta name="generator" content="PHPStorm, Sublime Text">
	<meta name="robots" content="all">

	<link rel="shortcut icon" title="Image" type="image/png" href="web/img/">
	<link rel="image_src" href="web/img/">
	<link rel="stylesheet" href="web/css/admin.css">
</head>
<body>
	<div class="main">
		<div class="container">
			<h1>::Espace Administration::</h1>
			
			<form class="form" method="POST" action="controllers/trt_connexionAdmin.php">
				<input type="email" name="email" placeholder="Email">
				<input type="password" name="password" placeholder="Mot de passe">
				<button type="submit" name="connexionAdmin">Connexion</button>
			</form>
		</div>
	</div>
</body>
</html>