// Cache barre de notification
cacherNotification = function (e) {
	document.getElementById("notification").style.display = 'none';
}

function handleFileSelect(evt) {
	var files = evt.target.files; // FileList object

	// Loop through the FileList and render image files as thumbnails.
	for (var i = 0, f; f = files[i]; i++) {

		// Only process image files.
		if (!f.type.match('image.*')) {
		continue;
		}

		var reader = new FileReader();

		// Ferme la capture de l'information du fichier
		reader.onload = (function(theFile) {
			return function(e) {
				var div = document.createElement('div');
				div.innerHTML = ['<img src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
				document.getElementById('profil').insertBefore(div, null);
			};
		})(f);

		// Lis l'image via le data URL
		reader.readAsDataURL(f);
	}
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);