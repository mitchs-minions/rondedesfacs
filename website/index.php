<?php

session_start();
include 'controllers/trt_course.php';

?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Accueil | Ronde Des Facs</title>

	<meta name="author" content="">
	<meta name="description" content="Ronde des facs">
	<meta name="geo.placename" content="Nice, PACA, Cote-d'Azur,France">
	<meta name="keywords" content="Ronde des facs,Etudiant,Université Sophia Antipolis,Course,4km">
	<meta name="copyright" content="">
	<meta name="generator" content="PHPStorm, Sublime Text">
	<meta name="robots" content="all">

	<link rel="shortcut icon" title="Image" type="image/png" href="web/img/">
	<link rel="image_src" href="web/img/">
	<link rel="stylesheet" href="web/css/style.css">
	<link rel="stylesheet" type="text/css" href="web/css/dataTables.css">
</head>

<body onload="setTimeout(cacherNotification,1500);">
	<?php

		require("controllers/userAgent.php");

		$details = getBrowser();

		if (isset($_GET["erreurC"])) {
			echo "La connexion a échoué";
		}

		if (isset($_GET["erreurI"])) {
			echo "L'inscription a échoué";
		}

		if (isset($_GET["nom"]) && isset($_GET["prenom"]) && isset($_GET["sexe"])) {
			$nom = $_GET["nom"];
			$prenom = $_GET["prenom"];
			$sexe = $_GET["sexe"];
		}

	?>

	<header>
		<h1 href="index.php">RDF</h1>
		<nav>
			<ul>
				<li>
					<?php 
						if (isset($_SESSION["email"]) && isset($_SESSION["password"])) {
							echo "Bonjour, ".$_SESSION["prenom"]." ".$_SESSION["nom"];
						} else {
							echo "Connectez-vous pour vous préinscrire à une course";
						}
					?>
				</li>
				<li><span id="profil"></span></li>
			</ul>
		</nav>
	</header>
	<main class="content">
		<noscript>
			Désolé, une erreur est survenue. Veuillez essayer d'actualiser la page.\n
			JavaScript est désactivé sur votre navigateur.<br>
			Vous ne pourrez pas vous inscrire sans cela, merci d'activer JavaScript dans votre navigateur.<br>
			Cliquez <a href="<?php echo "modules/".$details ?>.pdf">ici</a> pour savoir comment faire.<br><br>
		</noscript>
		<ul class="tabs">
			<li>
				<input type="radio" checked name="tabs" id="tab3">
				<label for="tab3">Courses</label>
				<div class="contenu">
					<?php if (isset($_GET["deco"])) {
						?> <div id="notification" class="valider">Vous êtes déconnecté</div> <?php
					} elseif (isset($_GET["co"])){
						?> <div id="notification" class="valider">Vous êtes connecté</div> <?php
					} elseif (isset($_GET["isco"])){
						?> <div id="notification" class="valider">Votre inscription a été effectué et vous êtes connecté</div> <?php
					} elseif (isset($_GET["modif"])){
						?> <div id="notification" class="valider">Vos informations ont été modifié, reconnectez vous</div> <?php
					} elseif (isset($_GET["inscrit"])){
						?> <div id="notification" class="valider">Votre inscription a été pris en compte</div> <?php
					} elseif (isset($_GET["desinscrit"])){
						?> <div id="notification" class="valider">Vous êtes désinscris de la course</div> <?php
					} elseif (isset($_GET["erreur"])){
						?> <div id="notification" class="valider">Une erreur est survenue</div> <?php
					}

					Course::getLesCourses();

					?>
					<br><center>Toute les courses disponibles</center><br><br>
					<table id="course" class="display" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Nom</th>
								<th>Date début</th>
								<th>Date Fin</th>
								<th>Date</th>
								<th>Nb Participant</th>
								<th>Distance</th>
								<?php if (isset($_SESSION["email"]) && isset($_SESSION["password"])) { ?>
									<th></th>
								<?php } ?>
							</tr>
						</thead>
					</table>
				</div>
			</li>

			<?php if (isset($_SESSION["email"]) && isset($_SESSION["password"])) { ?>

			<li>
				<input type="radio" name="tabs" id="tab4">
				<label for="tab4">Profil</label>
				<div class="contenu">
					<form action="controllers/trt_profil.php" method="POST" name="profilPass" id="profilPass">
						<center>
							Modifier votre
							<span>
								<input class="popup" type="password" id="password" name="password" pattern="[a-zA-Z0-9\s]{4,}" minlength="4" title="Le mot de passe doit être de 4 caractères alphanumériques minimum !" placeholder="Mot de passe" value="0000" onclick="this.value = ''" required /><legend for="password">Password</legend>
							</span>
							<input type="submit" id="modifierPass" name="modifierPass" value="Modifier"/>
						</center>
					</form>
					<form action="controllers/trt_profil.php" method="POST" name="profil" id="profil">
						<div class="boite2" style="margin-top:50px;">
							<li>
								<span>
									<input class="popup" type="email" pattern="[a-zA-Z0-9À-ŷ.!#$%&’*+/=?^_`{|}~-]+\@[a-zA-Z0-9]{4,}\.[a-zA-Z0-9]{2,4}" placeholder="Email" value="<?php echo $_SESSION["email"]?>" id="email" name="email" autofocus required /><legend for="email">Email</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" id="nom" name="nom" placeholder="Nom" value="<?php echo $_SESSION["nom"]?>" required /><legend for="nom">Nom</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" id="prenom" name="prenom" placeholder="Prénom" value="<?php echo $_SESSION["prenom"]?>" required /><legend for="prenom">Prenom</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="number" id="tel" name="tel" placeholder="Téléphone" value="<?php echo $_SESSION["tel"]?>"/><legend for="tel">Tel</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" id="adresse1" name="adresse1" placeholder="Adresse" value="<?php echo $_SESSION["adresse1"]?>"/><legend for="adresse1">Adresse</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" id="adresse2" name="adresse2" placeholder="Complémentaire" value="<?php echo $_SESSION["adresse2"]?>"/><legend for="adresse2">Adresse</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" id="ville" name="ville" placeholder="Ville" value="<?php echo $_SESSION["ville"]?>"/><legend for="ville">Ville</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="number" id="cp" name="cp" maxlength="5" placeholder="Postale" value="<?php echo $_SESSION["cp"]?>"/><legend for="cp">CPostale</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" id="region" name="region" placeholder="Région" value="<?php echo $_SESSION["region"]?>"/><legend for="region">Region</legend>
								</span>
							</li>
							<input type="submit" id="modifier" name="modifier" value="Modifier"/>
						</div>
					</form>
					<?php if (isset($_SESSION["email"]) && isset($_SESSION["password"])) {

						Course::getMesCourses();
					?>
						<div class="boite2" style="margin-top:500px;">
							<center>Toute vos courses</center><br>
							<table id="maCourse" class="display" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Course ID</th>
										<th>Certificat</th>
										<th>Annulation course</th>
									</tr>
								</thead>
							</table>
						</div>
					<?php } ?>
				</div>
			</li>
			<li>
				<input type="radio" name="tabs" id="deconnexion" onclick="location.href='controllers/fonction.php?logout'">
				<label for="deconnexion">Déconnexion</label>
			</li>

			<?php } else { ?>

			<li>
				<input type="radio" name="tabs" id="tab1" <?php if (isset($_GET["erreurC"])) { echo "checked"; } ?>>
				<label for="tab1">Connexion</label>
				<div class="contenu">
					<?php if (isset($_GET["erreurC"])) {
						?> <div id="notification" class="erreur">Une erreur est survenus veuillez réassayer</div> <?php
					} ?>
					<div class="boite">
						<form action="controllers/trt_connexion.php" method="POST" name="login" id="login">
							<li>
								<span>
									<input class="popup" type="email" pattern="[a-zA-Z0-9À-ŷ.!#$%&’*+/=?^_`{|}~-]+\@[a-zA-Z0-9]{4,}\.[a-zA-Z0-9]{2,4}" placeholder="Email" id="email" name="email" required /><legend for="email">Email</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="password" pattern="[a-zA-Z0-9\s]{4,}" minlength="4" title="Le mot de passe doit être de 4 caractères alphanumériques minimum !" placeholder="Mot de passe"  id="password" name="password" required /><legend for="password">Pass</legend>
								</span>
							</li>
							<input type="submit" id="connexion" name="connexion" value="Connexion"/>
						</form>
					</div>
				</div>
			</li>
			<li>
				<input type="radio" name="tabs" id="tab2" <?php if (isset($_GET["erreurI"])) { echo "checked"; } ?> >
				<label for="tab2">Inscription</label>
				<div class="contenu">
					<?php if (isset($_GET["erreurI"])) {
						?> <div id="notification" class="erreur">Une erreur est survenus veuillez réassayer</div> <?php
					} ?>
					<form action="controllers/trt_inscription.php" method="POST" name="signup" id="signup">
						<div class="boite3">
							<li>
								<span>
									<input class="popup" type="email" pattern="[a-zA-Z0-9À-ŷ.!#$%&’*+/=?^_`{|}~-]+\@[a-zA-Z0-9]{4,}\.[a-zA-Z0-9]{2,4}" placeholder="Email" id="email" name="email" autofocus required /><legend for="email">Email</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="password" pattern="[a-zA-Z0-9\s]{4,}" minlength="4" title="Le mot de passe doit être de 4 caractères alphanumériques minimum !" placeholder="Mot de passe"  id="password" name="password" value="<?php YTRjNjE3OThmNzNkM2Y2MTliZTFiYjNi ?>" required /><legend for="password">Pass</legend>
								</span>
							</li>
							<center>
								<?php

								if ($civilite == "M.") {
									?>
										<input type="radio" name="civilite" id="Monsieur" value="M." checked required> Monsieur
										<input type="radio" name="civilite" id="Madame" value="Mme." required> Madame
									<?php
								} elseif ($civilite = "Mme."){
									?>
										<input type="radio" name="civilite" id="Monsieur" value="M." required> Monsieur
										<input type="radio" name="civilite" id="Madame" value="Mme." checked required> Madame
									<?php
								} else {
									?>
										<input type="radio" name="civilite" id="homme" value="M." required> Monsieur
										<input type="radio" name="civilite" id="Madame" value="Mme." required> Madame
									<?php
								}

								?>
							</center>
						</div>
						<div class="boite2">
							<li>
								<span>
									<input class="popup" type="text" placeholder="Votre Nom"  id="nom" name="nom" value="<?php if (isset($_GET["erreurI"])) { echo $nom; } ?>" required /><legend for="nom">Nom</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" placeholder="Votre prénom"  id="prenom" name="prenom" value="<?php if (isset($_GET["erreurI"])) { echo $prenom; } ?>" required /><legend for="prenom">Prenom</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="number" placeholder="Tel"  id="tel" name="tel" /><legend for="tel">Tel</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" placeholder="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/société"  id="etablissement" name="etablissement" /><legend for="etablissement">Etablissement</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" placeholder="Votre adresse"  id="adresse1" name="adresse1" /><legend for="adresse1">Adresse</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" placeholder="Complémentaire"  id="adresse2" name="adresse2" /><legend for="adresse2">Adresse</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" placeholder="Votre ville"  id="ville" name="ville" /><legend for="ville">Ville</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="number" maxlength="5" placeholder=".........."  id="cp" name="cp" /><legend for="cp">CPostale</legend>
								</span>
							</li>
							<li>
								<span>
									<input class="popup" type="text" placeholder="Votre region"  id="region" name="region" /><legend for="region">Region</legend>
								</span>
							</li>
							<input type="submit" id="inscription" name="inscription" value="Inscription"/>
						</div>
					</form>
				</div>
			</li>

			<?php } ?>

		</ul>
	</main>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
	<script src="web/js/index.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#course').dataTable( {
				"ajax": "web/ajax/course.txt",

				"language": {
					"sProcessing":		"Traitement en cours...",
					"sSearch":			"Rechercher :",
					"sLengthMenu":		"Afficher _MENU_ courses",
					"sInfo":			"Affichage de la course _START_ à _END_ sur _TOTAL_ courses",
					"sInfoEmpty":		"Affichage de la course 0 à 0 sur 0 course",
					"sInfoFiltered":	"(filtré de _MAX_ course au total)",
					"sInfoPostFix":		"",
					"sLoadingRecords":	"Chargement en cours...",
					"sZeroRecords":		"Aucun course à afficher",
					"sEmptyTable":		"Aucune course disponible",
					"oPaginate": {
						"sFirst":		"Premier",
						"sPrevious":	"Précédent",
						"sNext":		"Suivant",
						"sLast":		"Dernier"
					},
				}

			});
		});

		<?php if (isset($_SESSION["email"]) && isset($_SESSION["password"])) { ?>

			$(document).ready(function() {
				$('#maCourse').dataTable( {
					"ajax": "web/ajax/courses<?php echo $_SESSION['id'] ?>.txt",

					"language": {
						"sProcessing":		"Traitement en cours...",
						"sSearch":			"Rechercher :",
						"sLengthMenu":		"",
						"sInfo":			"",
						"sInfoEmpty":		"",
						"sInfoFiltered":	"",
						"sInfoPostFix":		"",
						"sLoadingRecords":	"Chargement en cours...",
						"sZeroRecords":		"Aucun course à afficher",
						"sEmptyTable":		"Aucune course disponible",
						"oPaginate": {
							"sFirst":		"Premier",
							"sPrevious":	"Précédent",
							"sNext":		"Suivant",
							"sLast":		"Dernier"
						},
					}

				});
			});

		<?php } ?>
	</script>
</body>
</html>