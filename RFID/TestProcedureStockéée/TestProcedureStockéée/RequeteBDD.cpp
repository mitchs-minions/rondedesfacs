#include "RequeteBDD.h"

void RequeteBDD::appareillerCarte(char*pguid,int p_mem_id)
{
	Console::WriteLine("appareillerCarte");
	OdbcConnection^con = gcnew OdbcConnection("DSN=RondedesFacs;SERVER=127.0.0.1;UID=admin;PWD=admin;DRIVER=MySQL ODBC 5.3 ANSI Driver;DATABASE=rondedesfacs");
	con->Open();
	OdbcCommand^Cmd = gcnew OdbcCommand("{call appareiller_carte(?,?)}",con);

		Console::WriteLine("preparing request...");
		Cmd->Parameters->Add("p_guid",OdbcType::VarChar,20);
		Cmd->Parameters->default[0]->Value=gcnew String(pguid);
		Cmd->Parameters->Add("p_mem_id",OdbcType::VarChar,20);
		Cmd->Parameters->default[1]->Value=p_mem_id;
		
		Console::WriteLine("request ready\nexecuting request...");
		OdbcDataReader^myReader = Cmd->ExecuteReader();
		Console::WriteLine("ok");
	  while (myReader->Read())
	   {
		 Console::WriteLine();
		 for(Int32 i=0;i<myReader->FieldCount;i++)
		 {
			Console::WriteLine("{0}:{1}",(myReader->GetName(i))->ToString(),(myReader->GetValue(i))->ToString());
		 }
	   }
	   myReader->Close();
}

void RequeteBDD::ajouterPassage(char*pguid,int p_cou_id,int p_num_relais)
{
	Console::WriteLine("ajouterPassage");
	OdbcConnection^con = gcnew OdbcConnection("DSN=RondedesFacs;SERVER=127.0.0.1;UID=admin;PWD=admin;DRIVER=MySQL ODBC 5.3 ANSI Driver;DATABASE=rondedesfacs");
	con->Open();
	OdbcCommand^Cmd = gcnew OdbcCommand("{call ajouter_passage(?,?,?)}",con);

		Console::WriteLine("preparing request...");
		Cmd->Parameters->Add("p_guid",OdbcType::VarChar,20);
		Cmd->Parameters->default[0]->Value=gcnew String(pguid);
		Cmd->Parameters->Add("p_cou_id",OdbcType::Int);
		Cmd->Parameters->default[1]->Value=p_cou_id;
		Cmd->Parameters->Add("p_num_relais",OdbcType::Int);
		Cmd->Parameters->default[2]->Value=p_num_relais;
		/*Cmd->Parameters->Add("p_return",OdbcType::Bit);
		Cmd->Parameters->default[3]->Direction=ParameterDirection::Output;*/
		
		Console::WriteLine("request ready\nexecuting request...");
		OdbcDataReader^myReader = Cmd->ExecuteReader();
		Console::WriteLine("ok");
	  while (myReader->Read())
	   {
		 Console::WriteLine();
		 for(Int32 i=0;i<myReader->FieldCount;i++)
		 {
			Console::WriteLine("{0}:{1}",(myReader->GetName(i))->ToString(),(myReader->GetValue(i))->ToString());
		 }
	   }
	   myReader->Close();
}