<?php

require("config_init.php");

/**
* Permet de visualiser les courses
*/
class Course
{
	// Génère fichier text à afficher des courses disponibles
	static public function getLesCourses()
	{
		global $connexion;
		$lesCourses = $connexion->query("SELECT * FROM `COURSE`");

		if($lesCourses->rowCount() > 0) {
			$json = array();
			while($row = $lesCourses->fetch()){

				// Date courante
				$date1 = strtotime(date('Y-m-d'));
				// Date de déroulement de la course
				$date2 = strtotime($row['cou_date_ins_debut']);

				if ($date1 >= $date2) {
					// On ne récupère pas la course
				} else {
					$json[] = array(
						$row['cou_libelle'],
						$row['cou_date_ins_debut'],
						$row['cou_date_ins_fin'],
						$row['cou_date_deroulement'],
						$row['cou_participants_max'],
						$row['cou_distance'],
						"<input type='button' id='$row[cou_id]' name='inscription' value='Inscription' onclick=\"location.href='controllers/trt_preinscription.php?inscrCourse=$row[cou_id]'\">"
					);
				}
			}
			$response = array();
			$response['success'] = true;
			$response['aaData'] = $json;
			$dataJson = json_encode($response);
		} else {

			$json[] = array(
				"","","","Aucune course","","",""
			);
			$response = array();
			$response['success'] = false;
			$response['aaData'] = $json;
			$dataJson = json_encode($response);
		}

		$fichier = "web/ajax/course.txt";

		// Si fichier existe on le supprime
		if (file_exists($fichier)) {
			unlink($fichier);
		}

		$course = fopen($fichier, "w+");

		fgets($course); // On lit le fichier
		fwrite($course, $dataJson); // On écrit dans le fichier les résultats de la requête		
		fclose($course);
	}

	// Génère fichier text à afficher des courses du profil
	static public function getMesCourses()
	{
		global $connexion;
		$mesCourses = $connexion->query("SELECT * FROM `PARTICIPANT`, `COURSE` WHERE `par_mem_id` = ".$_SESSION['id']."");

		if($mesCourses->rowCount() > 0) {
			$json = array();
			while($row = $mesCourses->fetch()){

				if ($row['par_certificat'] == "") {
					$certificat = "<form name='formulaire' method='POST' action='controllers/trt_upload.php' enctype='multipart/form-data'><input type='file' id='$row[par_cou_id]' name='ajouter' value='Ajouter' required><br><input type='submit' name='upload' value='Upload'></form>";
				} else {
					$certificat = "Oui";
				}
				
				$json[] = array(
					$row['cou_libelle'],
					$certificat,
					"<input type='button' id='$row[par_cou_id]' name='annuler' value='Annuler' onclick=\"location.href='controllers/trt_preinscription.php?desinscrCourse=$row[par_cou_id]'\">"
				);
			}
			$response = array();
			$response['success'] = true;
			$response['aaData'] = $json;
			$dataJson = json_encode($response);
		} else {

			$json[] = array(
				"","Aucune course",""
			);
			$response = array();
			$response['success'] = false;
			$response['aaData'] = $json;
			$dataJson = json_encode($response);
		}

		$fichier = "web/ajax/courses".$_SESSION['id'].".txt";

		// Si fichier existe on le supprime
		if (file_exists($fichier)) {
			unlink($fichier);
		}

		$course = fopen($fichier, "w+");

		fgets($course); // On lit le fichier
		fwrite($course, $dataJson); // On écrit dans le fichier les résultats de la requête		
		fclose($course);
	}

	// Génère fichier text à afficher résultats courses
	static public function getLesResultats()
	{
		global $connexion;
		$resultat = $connexion->query("SELECT * FROM `RESULTATS`");

		if($resultat->rowCount() > 0) {
			$json = array();
			while($row = $resultat->fetch()){
				
				$json[] = array(
					$row['Rang'],
					$row['Prénom'],
					$row['Nom'],
					$row['Civilité'],
					$row['Ville'],
					$row['Etablissement'],
					$row['N° Dossard'],
					$row['Course'],
					$row['Nombre de passages'],
					$row['Temps']
				);
			}
			$response = array();
			$response['success'] = true;
			$response['aaData'] = $json;
			$dataJson = json_encode($response);
		} else {

			$json[] = array(
				"", "", "", "", "", "Aucun résultat", "", "", "", "",
			);
			$response = array();
			$response['success'] = false;
			$response['aaData'] = $json;
			$dataJson = json_encode($response);
		}

		$fichier = "web/ajax/resultats.txt";

		// Si fichier existe on le supprime
		if (file_exists($fichier)) {
			unlink($fichier);
		}

		$course = fopen($fichier, "w+");

		fgets($course); // On lit le fichier
		fwrite($course, $dataJson); // On écrit dans le fichier les résultats de la requête		
		fclose($course);
	}
}

?>