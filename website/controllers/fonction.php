<?php

// Détruit la session existante
function killSession()
{
	session_start();
	$_SESSION = array();
	session_regenerate_id(true);
	session_destroy();
}

// Redirige une fois déconnexion
if(isset($_GET['logout'])){
	killSession();
	header("Location: ../index.php?deco");
}

if (isset($_GET['modif'])) {
	killSession();
	header("Location: ../index.php?modif");
}

if (isset($_GET['deco'])) {
	killSession();
	header("Location: ../admin.php");
}

?>