<?php

// Initialisation de la session
session_start();
session_id();
header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: private");

// Erreur PHP
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

// Connexion au serveur MySQL
try {
	$dns = 'mysql:host=localhost;dbname=rondedesfacs';
	$utilisateur = 'admin';
	$motDePasse = 'admin';
	$connexion = new PDO( $dns, $utilisateur, $motDePasse );
	$connexion->query("SET NAMES UTF8");
	$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch ( Exception $e ) {
	echo "Connexion au Serveur MySQL impossible : ", $e->getMessage();
	die();
}

?>
