<?php

session_start();
include '../controllers/config_init.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Espace - Configuration</title>

		<meta name="author" content="">
		<meta name="description" content="Ronde des facs">
		<meta name="geo.placename" content="Nice, PACA, Cote-d'Azur,France">
		<meta name="keywords" content="Ronde des facs,Etudiant,Université Sophia Antipolis,Course,4km">
		<meta name="copyright" content="">
		<meta name="generator" content="PHPStorm, Sublime Text">
		<meta name="robots" content="all">

		<link rel="shortcut icon" title="Image" type="image/png" href="../web/img/">
		<link rel="image_src" href="../web/img/">
		<link rel="stylesheet" href="../web/css/espaceAdmin.css">
		<!--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css">-->
		<script type="text/javascript" src="../web/js/jquery.js"></script>
		<script type="text/javascript">
			$(document).ready( function() {
				/* Menu à afficher */
				$('.deroule_menu').click(function(){
					$(this).next().toggleClass('display_block');
				});
			});
		</script>
	</head>
	<body>
		<?php if (isset($_SESSION["email"]) && isset($_SESSION["password"]) && isset($_SESSION["libelle"])) { ?>
		<div class='main'>
			<div class='header'>
				<div class='title'>Espace <?php echo $_SESSION["libelle"]; ?></div>
				<div class='user'>
					<div class='name'>
						<a href="#" title="Mon profile"><?php echo $_SESSION["nom"]." ".$_SESSION["prenom"]; ?></a>
					</div>
					<div class='logout'>
						<a href="../controllers/fonction.php?deco" title="Déconnecter">Déconnecter</a>
					</div>
				</div>
			</div>
			<div class='menu'>
				<div class='nav'>
					<div class='search'>
						<input placeholder='Rechercher' type='text'>
					</div>
					<ul id='menu'>
						<?php

						switch ($_SESSION["libelle"]) {
							case 'Organisateur':
								echo "<li>
									<a class='deroule_menu'>Tableau de bord</a>
									<ul class='menu_cacher'>
										<li><a href='resultats.php''>Consultation résultats</a></li>
										<li><a href=''>Valider certificat</a></li>
										<li><a href='appareillerBadge.php'>Appareiller</a></li>
										<li><a href=''>Valider relais</a></li>
										<li><a href='extraireBDD.php'>Extraire</a></li>
									</ul>
								</li>";
								break;

							case 'Validateur':
								echo "<li>
									<a class='deroule_menu'>Tableau de bord</a>
									<ul class='menu_cacher'>
										<li><a href=''>Valider certificat</a></li>
										<li><a href=''>Valider relais</a></li>
									</ul>
								</li>";
								break;

							case 'Administrateur':
								echo "<li>
									<a class='deroule_menu'>Tableau de bord</a>
									<ul class='menu_cacher'>
										<li><a href='resultats.php'>Consultation résultats</a></li>
										<li><a href=''>Valider certificat</a></li>
										<li><a href=''>Appareiller</a></li>
										<li><a href=''>Valider relais</a></li>
										<li><a href=''>Extraire</a></li>
									</ul>
								</li>
								<li>
									<a class='deroule_menu'>Configuration</a>
									<ul class='menu_cacher'>
										<li><a href=''>Gérer membres</a></li>
										<li><a href=''>Inscription participant</a></li>
										<li><a href=''>Gérer participants</a></li>
									</ul>
								</li>";
								break;
						}

						?>
					</ul>
				</div>
				<div class='content'>
					<div class='title'>
						<a href="#" style="float: right;"></a>
					</div>
					<div class='grid' id='a1'>
						<div class='col'>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } else { header("Location: ../admin.php"); } ?>
	</body>
</html>

numéro de course et membre