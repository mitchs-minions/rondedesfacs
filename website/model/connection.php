<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 18/04/2015
 * Time: 16:43
 */
/*include_once("class.db.php");
$db = new db("mysql:host=127.0.0.1;port=8889;dbname=rondedesfacs", "root", "admin");
*/
header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: private");

// Erreur PHP
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

// Connexion au serveur MySQL
try {
    $dns = 'mysql:host=localhost;dbname=rondedesfacs';
    $utilisateur = 'root';
    $motDePasse = 'admin';
    $connexion = new PDO( $dns, $utilisateur, $motDePasse );
    $connexion->query("SET NAMES UTF8");
    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch ( Exception $e ) {
    echo "Connexion au Serveur MySQL impossible : ", $e->getMessage();
    die();
}

?>