<?php

session_start();
include '../controllers/config_init.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Espace - Configuration</title>

		<meta name="author" content="">
		<meta name="description" content="Ronde des facs">
		<meta name="geo.placename" content="Nice, PACA, Cote-d'Azur,France">
		<meta name="keywords" content="Ronde des facs,Etudiant,Université Sophia Antipolis,Course,4km">
		<meta name="copyright" content="">
		<meta name="generator" content="PHPStorm, Sublime Text">
		<meta name="robots" content="all">

		<link rel="shortcut icon" title="Image" type="image/png" href="../web/img/">
		<link rel="image_src" href="../web/img/">
		<link rel="stylesheet" href="../web/css/espaceAdmin.css">
		<!--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css">-->
		<script type="text/javascript" src="../web/js/jquery.js"></script>
		<script type="text/javascript">
			$(document).ready( function() {
				/* Menu à afficher */
				$('.deroule_menu').click(function(){
					$(this).next().toggleClass('display_block');
				});
			});
		</script>
	</head>
	<body>
		<?php if (isset($_SESSION["email"]) && isset($_SESSION["password"]) && isset($_SESSION["libelle"])) { ?>
		<div class='main'>
			<div class='header'>
				<div class='title'>Espace <?php echo $_SESSION["libelle"]; ?></div>
				<div class='user'>
					<div class='name'>
						<a href="#" title="Mon profile"><?php echo $_SESSION["nom"]." ".$_SESSION["prenom"]; ?></a>
					</div>
					<div class='logout'>
						<a href="../controllers/fonction.php?deco" title="Déconnecter">Déconnecter</a>
					</div>
				</div>
			</div>
			<div class='menu'>
				<div class='nav'>
					<div class='search'>
						<input placeholder='Rechercher' type='text'>
					</div>
					<ul id='menu'>
						<?php

						switch ($_SESSION["libelle"]) {
							case 'Organisateur':
								echo "<li>
									<a class='deroule_menu'>Tableau de bord</a>
									<ul class='menu_cacher'>
										<li><a href='resultats.php'>Consultation résultats</a></li>
										<li><a href='#'>Valider certificat</a></li>
										<li><a href='appareillerBadge.php'>Appareiller</a></li>
										<li><a href='#'>Valider relais</a></li>
										<li><a href='extraireBDD.php'>Extraire</a></li>
									</ul>
								</li>";
								break;

							case 'Validateur':
								echo "<li>
									<a class='deroule_menu'>Tableau de bord</a>
									<ul class='menu_cacher'>
										<li><a href='#'>Valider certificat</a></li>
										<li><a href='#'>Valider relais</a></li>
									</ul>
								</li>";
								break;

							case 'Administrateur':
								echo "<li>
									<a class='deroule_menu'>Tableau de bord</a>
									<ul class='menu_cacher'>
										<li><a href='resultats.php>Consultation résultats</a></li>
										<li><a href='#'>Valider certificat</a></li>
										<li><a href='#'>Appareiller</a></li>
										<li><a href='#'>Valider relais</a></li>
										<li><a href='#'>Extraire</a></li>
									</ul>
								</li>
								<li>
									<a class='deroule_menu'>Configuration</a>
									<ul class='menu_cacher'>
										<li><a href='#'>Gérer membres</a></li>
										<li><a href='#'>Inscription participant</a></li>
										<li><a href='#'>Gérer participants</a></li>
									</ul>
								</li>";
								break;
						}

						?>
					</ul>
				</div>
				<div class='content'>
					<div class='title'>
						Inscription participant	<a href="#" style="float: right;" class="light"></a>
					</div>
					<div class='grid'>
					<div class='col'>
						<div class='head'>Inscription manuel</div>
						<form method="post">
							<table><tbody>
								<tr>
									<td><label>Identifiant :</label></td>
									<td><input id="identifiant" name="identifiant" type="text" /></td>
								</tr>
								<tr>
									<td><label>Mot de passe :</label></td>
									<td><input required="required" id="identifiant" name="identifiant" type="text" /></td>
								</tr>
							</tbody></table>
							<div class='btnset'>
								<input class="btn pri" type="submit" name="submit" value="Valider" />
							</div>
						</form>

						<form method="post" action="espaceAdmin.php">

							<?php

							$requete = "SELECT * FROM `COURSE`";
							$sql = $connexion->query($requete);

							?> <select name='laCourse'> <?php

								while ($data = $sql->fetch(PDO::FETCH_ASSOC)) {

									echo "<option value='$data[cou_id]'>$data[cou_libelle]</option>";
								}?>
								
							</select>

							<div class='btnset'>
								<input class="btn pri" type="submit" name="extraire" value="Extraire" />
							</div>
						</form>

						<?php

						if (isset($_POST) && isset($_POST["extraire"])) {

							$extraction = $connexion->prepare("SELECT * FROM `PASSAGE` WHERE `pas_par_cou_id` = :idCourse");
							$extraction->bindValue("idCourse", $_POST["laCourse"]);
							$extraction->execute();
							$data = $extraction->fetchAll(PDO::FETCH_ASSOC);

							foreach ($data as $value) {
								$excel .= 'INSERT INTO `PASSAGE` (`pas_id`, `pas_par_mem_id`, `pas_par_cou_id`, `pas_temps`, `pas_log`) VALUES ("'.$value["pas_id"].'", "'.$value["pas_par_mem_id"].'", "'.$value["pas_par_cou_id"].'", "'.$value["pas_temps"].'", "'.$value["pas_log"].'");'."\n";
							}

							/*
							$excel = "INSERT INTO `rondedesfacs`.`PASSAGE` (`pas_id`, `pas_par_mem_id`, `pas_par_cou_id`, `pas_temps`, `pas_log`) VALUES "."\n";

							foreach ($data as $value) {
								$excel .= '("'.$value["pas_id"].'", "'.$value["pas_par_mem_id"].'", "'.$value["pas_par_cou_id"].'", "'.$value["pas_temps"].'", "'.$value["pas_log"].'"),'."\n";
							}
							
							$excel .= ";";
							*/

							$fichier = "../web/ajax/BDD.sql";

							// Si fichier existe on le supprime
							if (file_exists($fichier)) {
								unlink($fichier);
							}

							$extractionBDD = fopen($fichier, "w+");

							fgets($extractionBDD); // On lit le fichier
							fwrite($extractionBDD, $excel); // On écrit dans le fichier les résultats de la requête
							//exec("sed -r '$ s/.$//' "$fichier);
							fclose($extractionBDD);

							$file = "/var/www/rdf/website/web/ajax/BDD.sql";
							$filename = "BDDExtract.sql";
							header('Content-Description: File Transfer');
							header('Content-Type: application/sql');
							header('Content-Disposition: attachment; filename="'.$filename.'"');
							header('Content-Transfer-Encoding: binary');
							header('Expires: 0');
							header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
							header('Pragma: public');
							header('Content-Length: ' . filesize($file));
							ob_clean();
							flush();
							readfile($file);
							exit;
						}

						?>
					</div>
					</div>
				</div>
			</div>
		</div>
		<?php } else { header("Location: ../admin.php"); } ?>
	</body>
</html>

numéro de course et membre