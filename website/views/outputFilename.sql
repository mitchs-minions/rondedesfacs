-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: rondedesfacs
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `COURSE`
--

DROP TABLE IF EXISTS `COURSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COURSE` (
  `cou_id` int(11) NOT NULL AUTO_INCREMENT,
  `cou_libelle` varchar(50) DEFAULT NULL,
  `cou_date_ins_debut` date DEFAULT NULL,
  `cou_date_ins_fin` date DEFAULT NULL,
  `cou_date_deroulement` date DEFAULT NULL,
  `cou_participants_max` int(11) DEFAULT NULL,
  `cou_nb_relais` int(11) DEFAULT NULL,
  `cou_distance` int(11) DEFAULT NULL,
  `cou_sc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cou_id`),
  KEY `FK_COURSE_cou_sc_id` (`cou_sc_id`),
  CONSTRAINT `FK_COURSE_cou_sc_id` FOREIGN KEY (`cou_sc_id`) REFERENCES `STATUT_COURSE` (`sc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COURSE`
--

LOCK TABLES `COURSE` WRITE;
/*!40000 ALTER TABLE `COURSE` DISABLE KEYS */;
INSERT INTO `COURSE` VALUES (1,'Rondes des Facs 2013','2013-01-01','2013-03-01','2013-03-30',300,2,2,6),(2,'Rondes des Facs 2014','2014-01-01','2014-03-01','2014-03-30',300,2,2,7),(3,'Rondes des Facs 2015 Q1','2015-01-01','2014-03-01','2015-03-30',300,2,2,5),(4,'Rondes des Facs 2015 Q2','2015-01-01','2015-04-01','2015-05-12',300,2,2,4),(5,'Rondes des Facs 2015 Man','2015-01-01','2015-04-01','2015-09-01',300,2,2,3),(6,'Rondes des Facs 2015 Women','2015-01-01','2015-08-01','2015-11-01',300,2,2,2),(7,'Rondes des Facs 2016','2016-01-01','2016-04-01','2016-03-30',300,2,2,1);
/*!40000 ALTER TABLE `COURSE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MEMBRE`
--

DROP TABLE IF EXISTS `MEMBRE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEMBRE` (
  `mem_id` int(11) NOT NULL,
  `mem_guid` varchar(50) DEFAULT NULL,
  `mem_etablissement` varchar(50) DEFAULT NULL,
  `mem_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`mem_id`),
  CONSTRAINT `FK_MEMBRE_mem_id` FOREIGN KEY (`mem_id`) REFERENCES `PERSONNE` (`per_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MEMBRE`
--

LOCK TABLES `MEMBRE` WRITE;
/*!40000 ALTER TABLE `MEMBRE` DISABLE KEYS */;
INSERT INTO `MEMBRE` VALUES (1,NULL,'IUT de Sophia',1),(2,NULL,'IUT de Sophia',1),(3,NULL,'IUT de Sophia',1),(4,NULL,'IUT de Sophia',1),(5,NULL,'IUT de Sophia',1),(6,NULL,'IUT de Sophia',1),(7,NULL,'IUT de Sophia',1),(8,NULL,'IUT de Sophia',1),(9,NULL,'IUT de Sophia',1);
/*!40000 ALTER TABLE `MEMBRE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTICIPANT`
--

DROP TABLE IF EXISTS `PARTICIPANT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PARTICIPANT` (
  `par_mem_id` int(11) NOT NULL,
  `par_cou_id` int(11) NOT NULL,
  `par_num_dossard` int(11) NOT NULL,
  `par_certificat` varchar(60) DEFAULT NULL,
  `par_inscrit` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`par_mem_id`,`par_cou_id`),
  KEY `FK_PARTICIPANT_par_cou_id` (`par_cou_id`),
  CONSTRAINT `FK_PARTICIPANT_par_cou_id` FOREIGN KEY (`par_cou_id`) REFERENCES `COURSE` (`cou_id`),
  CONSTRAINT `FK_PARTICIPANT_par_mem_id` FOREIGN KEY (`par_mem_id`) REFERENCES `MEMBRE` (`mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PARTICIPANT`
--

LOCK TABLES `PARTICIPANT` WRITE;
/*!40000 ALTER TABLE `PARTICIPANT` DISABLE KEYS */;
INSERT INTO `PARTICIPANT` VALUES (1,1,1,NULL,1),(1,2,1,NULL,1),(1,3,1,NULL,1),(1,4,1,NULL,1),(1,5,1,NULL,1),(1,6,1,NULL,1),(1,7,1,NULL,0),(2,1,2,NULL,1),(2,2,2,NULL,1),(2,3,2,NULL,1),(2,4,2,NULL,1),(2,5,2,NULL,1),(2,6,2,NULL,1),(2,7,2,NULL,0),(3,1,3,NULL,1),(3,2,3,NULL,1),(3,3,3,NULL,1),(3,4,3,NULL,1),(3,5,3,NULL,1),(3,6,3,NULL,1),(3,7,3,NULL,0),(4,1,4,NULL,1),(4,2,4,NULL,1),(4,3,4,NULL,1),(4,4,4,NULL,1),(4,5,4,NULL,1),(4,6,4,NULL,1),(4,7,4,NULL,0),(5,1,5,NULL,1),(5,2,5,NULL,1),(5,3,5,NULL,1),(5,4,5,NULL,1),(5,5,5,NULL,1),(5,6,5,NULL,1),(5,7,5,NULL,0),(6,1,6,NULL,1),(6,2,6,NULL,1),(6,3,6,NULL,1),(6,4,6,NULL,1),(6,5,6,NULL,1),(6,6,6,NULL,1),(6,7,6,NULL,0),(7,1,7,NULL,1),(7,2,7,NULL,1),(7,3,7,NULL,1),(7,4,7,NULL,1),(7,5,7,NULL,1),(7,6,7,NULL,1),(7,7,7,NULL,0),(8,1,8,NULL,1),(8,2,8,NULL,1),(8,3,8,NULL,1),(8,4,8,NULL,1),(8,5,8,NULL,1),(8,6,8,NULL,1),(8,7,8,NULL,0),(9,1,9,NULL,1),(9,2,9,NULL,1),(9,3,9,NULL,1),(9,4,9,NULL,1),(9,5,9,NULL,1),(9,6,9,NULL,1),(9,7,9,NULL,0);
/*!40000 ALTER TABLE `PARTICIPANT` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER insb_participant BEFORE INSERT ON PARTICIPANT FOR EACH ROW
BEGIN
  DECLARE v_num_dossard INTEGER;
  SELECT par_num_dossard INTO v_num_dossard FROM PARTICIPANT WHERE par_cou_id = NEW.par_cou_id;

  IF v_num_dossard < 0 OR v_num_dossard IS NULL THEN
    SET NEW.par_num_dossard = 1;
  ELSE
    SET NEW.par_num_dossard = v_num_dossard + 1;
  END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `PASSAGE`
--

DROP TABLE IF EXISTS `PASSAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PASSAGE` (
  `pas_id` int(11) NOT NULL,
  `pas_par_mem_id` int(11) NOT NULL,
  `pas_par_cou_id` int(11) NOT NULL,
  `pas_temps` int(11) DEFAULT NULL,
  `pas_log` time DEFAULT NULL,
  PRIMARY KEY (`pas_id`,`pas_par_mem_id`,`pas_par_cou_id`),
  KEY `FK_PARTICIPANT_pas_par_mem_id` (`pas_par_mem_id`),
  KEY `FK_PARTICIPANT_pas_par_cou_id` (`pas_par_cou_id`),
  CONSTRAINT `FK_PARTICIPANT_pas_par_cou_id` FOREIGN KEY (`pas_par_cou_id`) REFERENCES `PARTICIPANT` (`par_cou_id`),
  CONSTRAINT `FK_PARTICIPANT_pas_par_mem_id` FOREIGN KEY (`pas_par_mem_id`) REFERENCES `PARTICIPANT` (`par_mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PASSAGE`
--

LOCK TABLES `PASSAGE` WRITE;
/*!40000 ALTER TABLE `PASSAGE` DISABLE KEYS */;
INSERT INTO `PASSAGE` VALUES (1,1,1,0,'10:00:00'),(1,1,2,0,'10:00:00'),(1,1,3,0,'10:00:00'),(1,2,1,0,'10:00:00'),(1,2,2,0,'10:00:00'),(1,3,1,0,'10:00:00'),(1,3,2,0,'10:00:00'),(1,4,1,0,'10:00:00'),(1,4,2,0,'10:00:00'),(1,5,1,0,'10:00:00'),(1,5,2,0,'10:00:00'),(1,6,1,0,'10:00:00'),(1,6,2,0,'10:00:00'),(1,7,1,0,'10:00:00'),(1,7,2,0,'10:00:00'),(1,8,1,0,'10:00:00'),(1,8,2,0,'10:00:00'),(1,9,1,0,'10:00:00'),(1,9,2,0,'10:00:00'),(2,1,1,600,'10:10:00'),(2,1,2,600,'10:10:00'),(2,1,3,0,'10:10:00'),(2,2,1,900,'10:15:00'),(2,2,2,900,'10:15:00'),(2,3,1,1500,'10:25:00'),(2,3,2,2400,'10:40:00'),(2,4,1,1800,'10:30:00'),(2,4,2,600,'10:10:00'),(2,5,1,900,'10:15:00'),(2,5,2,1800,'10:30:00'),(2,6,1,600,'10:10:00'),(2,6,2,600,'10:10:00'),(2,7,1,600,'10:10:00'),(2,7,2,1020,'10:17:00'),(2,8,1,1200,'10:20:00'),(2,8,2,600,'10:10:00'),(2,9,1,600,'10:10:00'),(2,9,2,600,'10:10:00'),(3,1,1,600,'10:20:00'),(3,1,2,600,'10:20:00'),(3,1,3,0,'10:30:00'),(3,2,1,600,'10:25:00'),(3,2,2,600,'10:25:00'),(3,3,1,900,'10:40:00'),(3,4,1,1500,'10:55:00'),(3,4,2,1200,'10:30:00'),(3,5,1,300,'10:20:00'),(3,5,2,600,'10:40:00'),(3,6,1,600,'10:20:00'),(3,6,2,1080,'10:28:00'),(3,7,1,1200,'10:30:00'),(3,7,2,540,'10:26:00'),(3,8,1,600,'10:30:00'),(3,8,2,600,'10:20:00'),(3,9,1,1200,'10:30:00'),(3,9,2,1200,'10:30:00'),(4,1,1,600,'10:30:00'),(4,1,2,600,'10:30:00'),(4,1,3,0,'10:50:00'),(4,2,1,600,'10:35:00'),(4,2,2,600,'10:35:00'),(4,4,2,600,'10:40:00'),(4,5,1,600,'10:30:00'),(4,5,2,600,'10:50:00'),(4,6,1,600,'10:30:00'),(4,6,2,1260,'10:49:00'),(4,7,1,600,'10:40:00'),(4,7,2,300,'10:31:00'),(4,8,1,600,'10:40:00'),(4,8,2,600,'10:30:00'),(4,9,1,1200,'10:50:00'),(4,9,2,1200,'10:50:00'),(5,1,1,600,'10:40:00'),(5,1,2,600,'10:40:00'),(5,1,3,0,'11:00:00'),(5,2,1,600,'10:45:00'),(5,2,2,600,'10:45:00'),(5,5,1,600,'10:40:00'),(5,6,1,600,'10:40:00'),(5,7,1,1200,'11:00:00'),(5,7,2,720,'10:43:00'),(5,8,1,1200,'11:00:00'),(5,8,2,1200,'10:50:00'),(5,9,1,600,'11:00:00'),(5,9,2,600,'11:00:00'),(6,1,1,600,'10:50:00'),(6,1,2,600,'10:50:00'),(6,2,1,890,'10:59:50'),(6,2,2,600,'10:55:00'),(6,5,1,600,'10:50:00'),(6,7,2,900,'10:58:00'),(6,8,2,600,'11:00:00'),(7,1,1,600,'11:00:00'),(7,1,2,600,'11:00:00'),(7,2,2,600,'11:05:00'),(7,5,1,600,'11:00:00');
/*!40000 ALTER TABLE `PASSAGE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PERSONNE`
--

DROP TABLE IF EXISTS `PERSONNE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERSONNE` (
  `per_id` int(11) NOT NULL AUTO_INCREMENT,
  `per_prenom` varchar(25) DEFAULT NULL,
  `per_nom` varchar(25) DEFAULT NULL,
  `per_civilite` char(5) DEFAULT NULL,
  `per_email` varchar(40) DEFAULT NULL,
  `per_tel` varchar(10) DEFAULT NULL,
  `per_adresse1` varchar(50) DEFAULT NULL,
  `per_adresse2` varchar(50) DEFAULT NULL,
  `per_ville` varchar(50) DEFAULT NULL,
  `per_cp` varchar(5) DEFAULT NULL,
  `per_region` varchar(50) DEFAULT NULL,
  `per_mdp` char(104) DEFAULT NULL,
  PRIMARY KEY (`per_id`),
  UNIQUE KEY `per_email` (`per_email`,`per_tel`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PERSONNE`
--

LOCK TABLES `PERSONNE` WRITE;
/*!40000 ALTER TABLE `PERSONNE` DISABLE KEYS */;
INSERT INTO `PERSONNE` VALUES (1,'John1','Smith1','M.','John.smith1@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(2,'John2','Smith2','M.','John.smith2@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(3,'John3','Smith3','M.','John.smith3@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(4,'John4','Smith4','M.','John.smith4@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(5,'John5','Smith5','M.','John.smith5@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(6,'John6','Smith6','M.','John.smith6@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(7,'John7','Smith7','M.','John.smith7@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(8,'John8','Smith8','M.','John.smith8@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(9,'John9','Smith9','M.','John.smith9@gmail.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(10,'Organisateur 1','','M.','org1@outlook.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(11,'Organisateur 2','','M.','org2@outlook.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(12,'Kevin','Simon','M.','kevin.simon@outlook.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452'),(13,'Alexandre','Breugnon','Dr.','alexandre.breugnon@doc.com','0760745758','70 AV Alfred Borriglione','Le Vercors 2','Nice','06200','Alpes Maritimes','0caccda4dabb1f62f34f219ab821b452');
/*!40000 ALTER TABLE `PERSONNE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STAFF`
--

DROP TABLE IF EXISTS `STAFF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STAFF` (
  `sta_id` int(11) NOT NULL,
  `sta_ts_id` int(11) NOT NULL,
  PRIMARY KEY (`sta_id`),
  KEY `FK_STAFF_sta_ts_id` (`sta_ts_id`),
  CONSTRAINT `FK_STAFF_sta_ts_id` FOREIGN KEY (`sta_ts_id`) REFERENCES `TYPE_STAFF` (`ts_id`),
  CONSTRAINT `FK_STAFF_sta_id` FOREIGN KEY (`sta_id`) REFERENCES `PERSONNE` (`per_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STAFF`
--

LOCK TABLES `STAFF` WRITE;
/*!40000 ALTER TABLE `STAFF` DISABLE KEYS */;
INSERT INTO `STAFF` VALUES (10,1),(11,1),(13,2),(12,3);
/*!40000 ALTER TABLE `STAFF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STATUT_COURSE`
--

DROP TABLE IF EXISTS `STATUT_COURSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STATUT_COURSE` (
  `sc_id` int(11) NOT NULL AUTO_INCREMENT,
  `sc_libelle` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`sc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STATUT_COURSE`
--

LOCK TABLES `STATUT_COURSE` WRITE;
/*!40000 ALTER TABLE `STATUT_COURSE` DISABLE KEYS */;
INSERT INTO `STATUT_COURSE` VALUES (1,'Prévue'),(2,'Inscriptions ouvertes'),(3,'Inscriptions fermées'),(4,'En cours'),(5,'Finie'),(6,'Résultats publiés'),(7,'Annulée');
/*!40000 ALTER TABLE `STATUT_COURSE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TYPE_STAFF`
--

DROP TABLE IF EXISTS `TYPE_STAFF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TYPE_STAFF` (
  `ts_id` int(11) NOT NULL AUTO_INCREMENT,
  `ts_libelle` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TYPE_STAFF`
--

LOCK TABLES `TYPE_STAFF` WRITE;
/*!40000 ALTER TABLE `TYPE_STAFF` DISABLE KEYS */;
INSERT INTO `TYPE_STAFF` VALUES (1,'Organisateur'),(2,'Validateur'),(3,'Administrateur');
/*!40000 ALTER TABLE `TYPE_STAFF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `V_COURSES`
--

DROP TABLE IF EXISTS `V_COURSES`;
/*!50001 DROP VIEW IF EXISTS `V_COURSES`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `V_COURSES` (
  `Course` tinyint NOT NULL,
  `Date de début d'inscription` tinyint NOT NULL,
  `Date de fin d'inscription` tinyint NOT NULL,
  `Date de déroulement` tinyint NOT NULL,
  `Nombre de relais` tinyint NOT NULL,
  `Distance entre relais` tinyint NOT NULL,
  `Statut` tinyint NOT NULL,
  `Nombre de participants (broken)` tinyint NOT NULL,
  `Nombre de participants max` tinyint NOT NULL,
  `Nombre d'inscrit` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `V_PARTICIPANTS`
--

DROP TABLE IF EXISTS `V_PARTICIPANTS`;
/*!50001 DROP VIEW IF EXISTS `V_PARTICIPANTS`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `V_PARTICIPANTS` (
  `N° dossard` tinyint NOT NULL,
  `Prénom` tinyint NOT NULL,
  `Nom` tinyint NOT NULL,
  `Civilité` tinyint NOT NULL,
  `Ville` tinyint NOT NULL,
  `Code Postal` tinyint NOT NULL,
  `Région` tinyint NOT NULL,
  `Etablissement` tinyint NOT NULL,
  `Course` tinyint NOT NULL,
  `Inscription validée` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `V_RESULTATS`
--

DROP TABLE IF EXISTS `V_RESULTATS`;
/*!50001 DROP VIEW IF EXISTS `V_RESULTATS`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `V_RESULTATS` (
  `Rang (Broken)` tinyint NOT NULL,
  `Prénom` tinyint NOT NULL,
  `Nom` tinyint NOT NULL,
  `Civilité` tinyint NOT NULL,
  `Ville` tinyint NOT NULL,
  `Code Postal` tinyint NOT NULL,
  `Région` tinyint NOT NULL,
  `Etablissement` tinyint NOT NULL,
  `N° dossard` tinyint NOT NULL,
  `Course` tinyint NOT NULL,
  `Nombre de passages` tinyint NOT NULL,
  `Temps` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `V_COURSES`
--

/*!50001 DROP TABLE IF EXISTS `V_COURSES`*/;
/*!50001 DROP VIEW IF EXISTS `V_COURSES`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `V_COURSES` AS select `COURSE`.`cou_libelle` AS `Course`,`COURSE`.`cou_date_ins_debut` AS `Date de début d'inscription`,`COURSE`.`cou_date_ins_fin` AS `Date de fin d'inscription`,`COURSE`.`cou_date_deroulement` AS `Date de déroulement`,`COURSE`.`cou_nb_relais` AS `Nombre de relais`,`COURSE`.`cou_distance` AS `Distance entre relais`,`STATUT_COURSE`.`sc_libelle` AS `Statut`,count((locate(1,`PARTICIPANT`.`par_inscrit`) > 0)) AS `Nombre de participants (broken)`,`COURSE`.`cou_participants_max` AS `Nombre de participants max`,count(`PARTICIPANT`.`par_cou_id`) AS `Nombre d'inscrit` from ((`PARTICIPANT` join `COURSE`) join `STATUT_COURSE`) where ((`PARTICIPANT`.`par_cou_id` = `COURSE`.`cou_id`) and (`COURSE`.`cou_sc_id` = `STATUT_COURSE`.`sc_id`)) group by `COURSE`.`cou_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `V_PARTICIPANTS`
--

/*!50001 DROP TABLE IF EXISTS `V_PARTICIPANTS`*/;
/*!50001 DROP VIEW IF EXISTS `V_PARTICIPANTS`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `V_PARTICIPANTS` AS select `PARTICIPANT`.`par_num_dossard` AS `N° dossard`,`PERSONNE`.`per_prenom` AS `Prénom`,`PERSONNE`.`per_nom` AS `Nom`,`PERSONNE`.`per_civilite` AS `Civilité`,`PERSONNE`.`per_ville` AS `Ville`,`PERSONNE`.`per_cp` AS `Code Postal`,`PERSONNE`.`per_region` AS `Région`,`MEMBRE`.`mem_etablissement` AS `Etablissement`,`COURSE`.`cou_libelle` AS `Course`,`PARTICIPANT`.`par_inscrit` AS `Inscription validée` from (((`PERSONNE` join `MEMBRE`) join `PARTICIPANT`) join `COURSE`) where ((`PERSONNE`.`per_id` = `MEMBRE`.`mem_id`) and (`PARTICIPANT`.`par_mem_id` = `MEMBRE`.`mem_id`) and (`PARTICIPANT`.`par_cou_id` = `COURSE`.`cou_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `V_RESULTATS`
--

/*!50001 DROP TABLE IF EXISTS `V_RESULTATS`*/;
/*!50001 DROP VIEW IF EXISTS `V_RESULTATS`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `V_RESULTATS` AS select 'Rang (Broken)' AS `Rang (Broken)`,`PERSONNE`.`per_prenom` AS `Prénom`,`PERSONNE`.`per_nom` AS `Nom`,`PERSONNE`.`per_civilite` AS `Civilité`,`PERSONNE`.`per_ville` AS `Ville`,`PERSONNE`.`per_cp` AS `Code Postal`,`PERSONNE`.`per_region` AS `Région`,`MEMBRE`.`mem_etablissement` AS `Etablissement`,`PARTICIPANT`.`par_num_dossard` AS `N° dossard`,`COURSE`.`cou_libelle` AS `Course`,count(`PASSAGE`.`pas_id`) AS `Nombre de passages`,sec_to_time(sum(`PASSAGE`.`pas_temps`)) AS `Temps` from ((((`PERSONNE` join `MEMBRE`) join `PARTICIPANT`) join `COURSE`) join `PASSAGE`) where ((`PERSONNE`.`per_id` = `MEMBRE`.`mem_id`) and (`PARTICIPANT`.`par_mem_id` = `MEMBRE`.`mem_id`) and (`PARTICIPANT`.`par_cou_id` = `COURSE`.`cou_id`) and (`PASSAGE`.`pas_par_mem_id` = `PARTICIPANT`.`par_mem_id`) and (`PASSAGE`.`pas_par_cou_id` = `PARTICIPANT`.`par_cou_id`)) group by `PASSAGE`.`pas_par_mem_id`,`PASSAGE`.`pas_par_cou_id` order by `COURSE`.`cou_libelle`,count(`PASSAGE`.`pas_id`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-12 15:13:25
